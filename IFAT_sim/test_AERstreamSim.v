`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:25:00 02/20/2013
// Design Name:   AERstreamSim
// Module Name:   C:/HDL/git/IFAT_state_machines/test_AERstreamSim.v
// Project Name:  IFAT_state_machine_test
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: AERstreamSim
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_AERstreamSim;

	// Inputs
	reg acknowledge;
	reg clock;
	reg reset;

	// Outputs
	wire [31:0] AddressEvent;
	wire request;

	// Instantiate the Unit Under Test (UUT)
	AERstreamSim #(20, 1, 1, 0, -1) 
  uut(
		.AddressEvent(AddressEvent), 
		.request(request), 
		.acknowledge(acknowledge), 
		.clock(clock), 
		.reset(reset)
	);

	initial begin
		// Initialize Inputs
    acknowledge = 0;
		clock = 0;
		reset = 1;
		#10 reset = 0;  // Wait 10 ns for global reset to finish
    #20000 $stop;
	end

  always @(request)
  begin
    #4 acknowledge = request;
  end
endmodule

