`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    17:11:42 03/15/2010
// Design Name:
// Module Name:    analog_arbit
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies: Priority_buffer, queue_backend
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module input_to_al(
  clk, rst,
  /*
  INSERT THE Pins for EXTERNAL CONTROL !
  */
  req_dl,
  ack_dl,
  fifo_in_dl,
  if0_out_al,
  if0_en,
  if1_out_al,
  if1_en,
  if2_out_al,
  if2_en,
  if3_out_al,
  if3_en
  );

input rst, clk; // global reset and clock

input req_dl; // AER request from digital layer
output ack_dl;  // AER acknowledge to digital layer
input [20:0] fifo_in_dl;  // AER input from digital layer: [20:6]: address; [5:0]: weight
// synapse addresses
output [12:0] if0_out_al;
output [12:0] if1_out_al;
output [12:0] if2_out_al;
output [12:0] if3_out_al;
output if0_en, if1_en, if2_en, if3_en;  // synapse AER enable, pulse width modulation signal to neuron

reg ack_dl;
reg [18:0] din0, din1, din2, din3;
reg req0, req1, req2, req3;
wire pop_req0, pop_req1, pop_req2, pop_req3;
wire [21:0] dout0, dout1, dout2, dout3;

wire [1:0] tile;
assign tile = fifo_in_dl[20:19];

Priority_buffer 
  P_buff0(.clk(clk), .rst(rst), .data_in({3'b0, din0}), .request(req0), 
    .pop_request(pop_req0), .data_out(dout0), .output_request_d(op_req0), .ack(bfull0)),
  P_buff1(.clk(clk), .rst(rst), .data_in({3'b0, din1}), .request(req1), 
    .pop_request(pop_req1), .data_out(dout1), .output_request_d(op_req1), .ack(bfull1)),
  P_buff2(.clk(clk), .rst(rst), .data_in({3'b0, din2}), .request(req2), 
    .pop_request(pop_req2), .data_out(dout2), .output_request_d(op_req2), .ack(bfull2)),
  P_buff3(.clk(clk), .rst(rst), .data_in({3'b0, din3}), .request(req3), 
    .pop_request(pop_req3), .data_out(dout3), .output_request_d(op_req3), .ack(bfull3));

queue_backend 
  qback0(.clk(clk), .rst(rst), .op_req(op_req0), .address_event(dout0), 
         .pop_req(pop_req0), .if_out_al(if0_out_al), .if_en(if0_en)),
  qback1(.clk(clk), .rst(rst), .op_req(op_req1), .address_event(dout1),
         .pop_req(pop_req1), .if_out_al(if1_out_al), .if_en(if1_en)),
  qback2(.clk(clk), .rst(rst), .op_req(op_req2), .address_event(dout2),
         .pop_req(pop_req2), .if_out_al(if2_out_al), .if_en(if2_en)),
  qback3(.clk(clk), .rst(rst), .op_req(op_req3), .address_event(dout3),
         .pop_req(pop_req3), .if_out_al(if3_out_al), .if_en(if3_en));

/* assign the different 4 FIFO based on what the data value is */
always @(bfull0, bfull1, bfull2, bfull3, req_dl, tile, fifo_in_dl)
if (req_dl == 1'b1) /* depending on what the data term says - connect to required fifo */
  begin
    req0 = (2'h0 == tile)? req_dl : 1'b0;
    req1 = (2'h1 == tile)? req_dl : 1'b0;
    req2 = (2'h2 == tile)? req_dl : 1'b0;
    req3 = (2'h3 == tile)? req_dl : 1'b0;

    din0 = (2'h0 == tile)? fifo_in_dl[18:0] : 19'b0;
    din1 = (2'h1 == tile)? fifo_in_dl[18:0] : 19'b0;
    din2 = (2'h2 == tile)? fifo_in_dl[18:0] : 19'b0;
    din3 = (2'h3 == tile)? fifo_in_dl[18:0] : 19'b0;

    case (fifo_in_dl[20:19])
    0:
      ack_dl = bfull0;
    1:
      ack_dl = bfull1;
    2:
      ack_dl = bfull2;
    3:
      ack_dl = bfull3;
    endcase
  end
else
  begin /* handshake and data lines to zero */
    req0 = 1'b0;
    req1 = 1'b0;
    req2 = 1'b0;
    req3 = 1'b0;

    din0 = 19'b0;
    din1 = 19'b0;
    din2 = 19'b0;
    din3 = 19'b0;

    ack_dl = 1'b0;
  end

endmodule
