`timescale 1ns/1ps

module reg22x16(out_data, out_data1, in_data, read_location, write_location, enable_write, enable_read);
output [21:0] out_data, out_data1;
input [21:0] in_data;
input [15:0] read_location, write_location;
input enable_write, enable_read;

wire [15:0] read_enable, write_enable;

wire [21:0] out_data, out_data1;


assign write_enable = write_location & {16{enable_write}};
assign read_enable = read_location & {16{enable_read}};

register_22bits reg00(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[0]), .write_enable(write_enable[0]));
register_22bits reg01(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[1]), .write_enable(write_enable[1]));
register_22bits reg02(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[2]), .write_enable(write_enable[2]));
register_22bits reg03(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[3]), .write_enable(write_enable[3]));

register_22bits reg04(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[4]), .write_enable(write_enable[4]));
register_22bits reg05(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[5]), .write_enable(write_enable[5]));
register_22bits reg06(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[6]), .write_enable(write_enable[6]));
register_22bits reg07(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[7]), .write_enable(write_enable[7]));

register_22bits reg08(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[8]), .write_enable(write_enable[8]));
register_22bits reg09(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[9]), .write_enable(write_enable[9]));
register_22bits reg10(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[10]), .write_enable(write_enable[10]));
register_22bits reg11(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[11]), .write_enable(write_enable[11]));

register_22bits reg12(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[12]), .write_enable(write_enable[12]));
register_22bits reg13(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[13]), .write_enable(write_enable[13]));
register_22bits reg14(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[14]), .write_enable(write_enable[14]));
register_22bits reg15(.out_data(out_data), .out_data1(out_data1), .in_data(in_data), .read_enable(read_enable[15]), .write_enable(write_enable[15]));

endmodule


