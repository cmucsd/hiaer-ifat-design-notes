
`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
//
// Design Name:   top_port
// Project Name:  neovision_test
//
////////////////////////////////////////////////////////////////////////////////

module tb_top_input3_rtl_full;

// Inputs
reg clk;
reg rst;
reg ext_IFAT;
reg ext_HIAER;
reg addr;
reg [10:0] if0_din0;
reg if0_req0;
reg [10:0] if0_din1;
reg if0_req1;
reg [10:0] if0_din2;
reg if0_req2;
reg [10:0] if0_din3;
reg if0_req3;
reg [10:0] if1_din0;
reg if1_req0;
reg [10:0] if1_din1;
reg if1_req1;
reg [10:0] if1_din2;
reg if1_req2;
reg [10:0] if1_din3;
reg if1_req3;
reg [20:0] digi_in0;
reg digi_req0;
reg digio_ack0;
reg [20:0] digi_in1;
reg digi_req1;
reg digio_ack1;
reg [22:0] ext_in;
reg ext_inreq;
reg ext_outack;

reg flag_phase2;

// Outputs
wire [12:0] if0_dout0;
wire if0_ack0;
wire if0_pulse0;
wire [12:0] if0_dout1;
wire if0_ack1;
wire if0_pulse1;
wire [12:0] if0_dout2;
wire if0_ack2;
wire if0_pulse2;
wire [12:0] if0_dout3;
wire if0_ack3;
wire if0_pulse3;
wire [12:0] if1_dout0;
wire if1_ack0;
wire if1_pulse0;
wire [12:0] if1_dout1;
wire if1_ack1;
wire if1_pulse1;
wire [12:0] if1_dout2;
wire if1_ack2;
wire if1_pulse2;
wire [12:0] if1_dout3;
wire if1_ack3;
wire if1_pulse3;
wire digi_ack0;
wire [12:0] digi_out0;
wire digio_req0;
wire digi_ack1;
wire [12:0] digi_out1;
wire digio_req1;
wire ext_inack;
wire [22:0] ext_out;
wire ext_outreq;

integer i;

////////////////////////////////////////////////////////////////////////////////
// Instantiate the Unit Under Test (UUT)
////////////////////////////////////////////////////////////////////////////////

IFAT_AER_connect uut (
		.clk(clk), 
		.rst(rst), 
		.ext_IFAT(ext_IFAT), 
		.ext_HIAER(ext_HIAER), 
		.addr(addr),
		.if0_din0(if0_din0), 
		.if0_dout0(if0_dout0), 
		.if0_req0(if0_req0), 
		.if0_ack0(if0_ack0), 
		.if0_pulse0(if0_pulse0), 
		.if0_din1(if0_din1), 
		.if0_dout1(if0_dout1), 
		.if0_req1(if0_req1), 
		.if0_ack1(if0_ack1), 
		.if0_pulse1(if0_pulse1), 
		.if0_din2(if0_din2), 
		.if0_dout2(if0_dout2), 
		.if0_req2(if0_req2), 
		.if0_ack2(if0_ack2), 
		.if0_pulse2(if0_pulse2), 
		.if0_din3(if0_din3), 
		.if0_dout3(if0_dout3), 
		.if0_req3(if0_req3), 
		.if0_ack3(if0_ack3), 
		.if0_pulse3(if0_pulse3), 
		.if1_din0(if1_din0), 
		.if1_dout0(if1_dout0), 
		.if1_req0(if1_req0), 
		.if1_ack0(if1_ack0), 
		.if1_pulse0(if1_pulse0), 
		.if1_din1(if1_din1), 
		.if1_dout1(if1_dout1), 
		.if1_req1(if1_req1), 
		.if1_ack1(if1_ack1), 
		.if1_pulse1(if1_pulse1), 
		.if1_din2(if1_din2), 
		.if1_dout2(if1_dout2), 
		.if1_req2(if1_req2), 
		.if1_ack2(if1_ack2), 
		.if1_pulse2(if1_pulse2), 
		.if1_din3(if1_din3), 
		.if1_dout3(if1_dout3), 
		.if1_req3(if1_req3), 
		.if1_ack3(if1_ack3), 
		.if1_pulse3(if1_pulse3), 
		.IFAT_data_out_0(digi_in0), 
		.IFAT_req_out_0(digi_req0), 
		.ack_IFAT_0(digi_ack0), 
		.data_IFAT_0(digi_out0), 
		.request_IFAT_0(digio_req0), 
		.ack_from_IFAT_0(digio_ack0), 
		.IFAT_data_out_1(digi_in1), 
		.IFAT_req_out_1(digi_req1), 
		.ack_IFAT_1(digi_ack1), 
		.data_IFAT_1(digi_out1), 
		.request_IFAT_1(digio_req1), 
		.ack_from_IFAT_1(digio_ack1), 
		.ext_in(ext_in), 
		.ext_inack(ext_inack), 
		.ext_inreq(ext_inreq), 
		.ext_out(ext_out), 
		.ext_outack(ext_outack), 
		.ext_outreq(ext_outreq)
);

////////////////////////////////////////////////////////////////////////////////
// Input initialization
////////////////////////////////////////////////////////////////////////////////

initial begin
	// Initialize Inputs
        clk = 1'b0;

	ext_IFAT = 0;
	ext_HIAER = 0;

	addr = 1'b0;

	if0_din0 = 0;
	if0_req0 = 0;
	if0_din1 = 0;
	if0_req1 = 0;
	if0_din2 = 0;
	if0_req2 = 0;
	if0_din3 = 0;
	if0_req3 = 0;
	if1_din0 = 0;
	if1_req0 = 0;
	if1_din1 = 0;
	if1_req1 = 0;
	if1_din2 = 0;
	if1_req2 = 0;
	if1_din3 = 0;
	if1_req3 = 0;

	digi_in0 = 0;
	digi_req0 = 0;
	digio_ack0 = 0;
	digi_in1 = 0;
	digi_req1 = 0;
	digio_ack1 = 0;

	ext_in = 0;
	ext_inreq = 0;
	ext_outack = 0;
end

////////////////////////////////////////////////////////////////////////////////
// Initialize reset and clock
////////////////////////////////////////////////////////////////////////////////

initial
begin	
	rst = 1'b1;
        flag_phase2 = 1'b0;
#160;
        rst = 1'b0;
end


////////////////////////////////////////////////////////////////////////////////
// Drive the clock
////////////////////////////////////////////////////////////////////////////////

always
begin
	#10 clk =  ~clk;
end

////////////////////////////////////////////////////////////////////////////////
// Stimuli from FPGA
////////////////////////////////////////////////////////////////////////////////

initial
begin

// wait for reset time to finish
# 160;

////////////////////////////////////////////////////
// configure in ext_IFAT mode
////////////////////////////////////////////////////

#30;
	ext_IFAT <= 1'b0;
	ext_HIAER <=1'b0;
#60;
	ext_IFAT <= 1'b1;
	ext_HIAER <=1'b0;

////////////////////////////////////////////////////
// drive external inputs from FPGA
// these are assumed concentrated by the arbiter
////////////////////////////////////////////////////

////////////////////////////////////////////////////
// Phase 1: Generic request sequences
////////////////////////////////////////////////////

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0011_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#26;
	ext_inreq<= 1'b0;

#20;

#210;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_1;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#410;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#90; // changed from 210 to 30 if this works go down from here.
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60; // changed to 60 from 400, if it works, change all 400 to 60
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
// #60; // QQQ Srinjoy 12/20/12
#20;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;
	ext_in <= 23'b00_0010_1010_1011_0110_0001_1;
	ext_inreq<= 1'b0;
#21; 					
	ext_inreq<= 1'b1;
#19;
	ext_inreq<= 1'b0;

#20;

#0;
	ext_in <= 23'b00_0010_1010_1011_0110_0000_0;	
	ext_inreq<= 1'b0;
#30; 					
	ext_inreq<= 1'b1;
#20;
	ext_inreq<= 1'b0;

#60;

# 500;

////////////////////////////////////////////////////
// Phase 2: One clock alternating request sequences
////////////////////////////////////////////////////

//	ext_in <= 23'b00_0010_1010_1011_0110_0011_1;

        flag_phase2 <= 1'b1;
        ext_in[22:6] <= 17'b00_0010_1010_1011_011;
        ext_in[5:0]  <= 6'b111111;

        for (i=0;i<=50;i=i+1)
        begin
	 ext_inreq<= 1'b0;
#20; 					
	 ext_inreq<= 1'b1;
#20;
	 ext_inreq<= 1'b0;
        end

$stop;

end		

endmodule
