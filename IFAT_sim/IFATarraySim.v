`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    18:51:03 02/14/2013 
// Design Name:    HiAER-IFAT v.1.0
// Module Name:    IFATarraySim 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Simulate IFAT array output spikes
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module IFATarraySim(address, req, ack);
  parameter delay = 40, setup = 2, hold = 3, block = 0, seed = 0;

  input ack;
  output reg req;
  output [10:0] address;

  reg [10:0] count;

  genvar i;
  generate for(i=0; i<11; i=i+1)
    begin:BITREVERSE
      assign address[i] = count[10-i];
    end
  endgenerate

initial
begin
  count <= seed; 
  req <= 1'b0;
end

always @(negedge ack)
begin
  #(delay);  // delay
  count <= count + 1;
  #(setup);   // setup
  req <= 1'b1;
  $display("Time %d: Address event %h generated in block %d", $time, address, block);
end

always @(posedge ack)
begin
  #(hold); // hold
  req <= 1'b0;
end

endmodule
