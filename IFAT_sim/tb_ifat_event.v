`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    08:57:56 02/19/2013 
// Design Name: 
// Module Name:    tb_ifat_event 
// Design Name:    HiAER-IFAT v.1.0
// Target Devices: 
// Tool versions: 
// Description: Simulates analog events from the IFAT sent to the FPGA 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module tb_ifat_event();

// global control
reg clk;
reg reset;

// IFAT interface controller clock
reg fpgaclk;

// external mode: hardwired (unless we want to do loopback)
wire ext_IFAT;
wire ext_HIAER;
assign ext_HIAER = 1'b0;
assign ext_IFAT = 1'b1;

// address events from IFAT arrays
wire [10:0] if00_din0;
wire if00_req0;
wire if00_ack0;

wire [10:0] if00_din1;
wire if00_req1;
wire if00_ack1;

wire [10:0] if00_din2;
wire if00_req2;
wire if00_ack2;

wire [10:0] if00_din3;
wire if00_req3;
wire if00_ack3;


wire [10:0] if01_din0;
wire if01_req0;
wire if01_ack0;

wire [10:0] if01_din1;
wire if01_req1;
wire if01_ack1;

wire [10:0] if01_din2;
wire if01_req2;
wire if01_ack2;

wire [10:0] if01_din3;
wire if01_req3;
wire if01_ack3;


wire [10:0] if10_din0;
wire if10_req0;
wire if10_ack0;

wire [10:0] if10_din1;
wire if10_req1;
wire if10_ack1;

wire [10:0] if10_din2;
wire if10_req2;
wire if10_ack2;

wire [10:0] if10_din3;
wire if10_req3;
wire if10_ack3;


wire [10:0] if11_din0;
wire if11_req0;
wire if11_ack0;

wire [10:0] if11_din1;
wire if11_req1;
wire if11_ack1;

wire [10:0] if11_din2;
wire if11_req2;
wire if11_ack2;

wire [10:0] if11_din3;
wire if11_req3;
wire if11_ack3;


// address events to IFAT arrays
wire [12:0] if00_dout0;
wire if00_pulse0;

wire [12:0] if00_dout1;
wire if00_pulse1;

wire [12:0] if00_dout2;
wire if00_pulse2;

wire [12:0] if00_dout3;
wire if00_pulse3;


wire [12:0] if01_dout0;
wire if01_pulse0;

wire [12:0] if01_dout1;
wire if01_pulse1;

wire [12:0] if01_dout2;
wire if01_pulse2;

wire [12:0] if01_dout3;
wire if01_pulse3;


wire [12:0] if10_dout0;
wire if10_pulse0;

wire [12:0] if10_dout1;
wire if10_pulse1;

wire [12:0] if10_dout2;
wire if10_pulse2;

wire [12:0] if10_dout3;
wire if10_pulse3;


wire [12:0] if11_dout0;
wire if11_pulse0;

wire [12:0] if11_dout1;
wire if11_pulse1;

wire [12:0] if11_dout2;
wire if11_pulse2;

wire [12:0] if11_dout3;
wire if11_pulse3;


// digital ports to chip stack (not used)

wire [20:0] digi0_in0;
wire digi0_inreq0;
wire digi0_inack0;

assign digi0_in0 = 21'bX;
assign digi0_inreq0 = 1'bX;

wire [12:0] digi0_out0;
wire digi0_outreq0;
wire digi0_outack0;

assign digi0_outack0 = 1'bX;


wire [20:0] digi0_in1;
wire digi0_inreq1;
wire digi0_inack1;

assign digi0_in1 = 21'bX;
assign digi0_inreq1 = 1'bX;

wire [12:0] digi0_out1;
wire digi0_outreq1;
wire digi0_outack1;

assign digi0_outack1 = 1'bX;


wire [20:0] digi1_in0;
wire digi1_inreq0;
wire digi1_inack0;

assign digi1_in0 = 21'bX;
assign digi1_inreq0 = 1'bX;

wire [12:0] digi1_out0;
wire digi1_outreq0;
wire digi1_outack0;

assign digi1_outack0 = 1'bX;


wire [20:0] digi1_in1;
wire digi1_inreq1;
wire digi1_inack1;

assign digi1_in1 = 21'bX;
assign digi1_inreq1 = 1'bX;

wire [12:0] digi1_out1;
wire digi1_outreq1;
wire digi1_outack1;

assign digi1_outack1 = 1'bX;

// external digital port (this is the one we're using)

wire [22:0] ext_in;
wire ext_inreq;
wire ext_inack;

wire [22:0] ext_out;
wire ext_outreq;
wire ext_outack;


////////////////////////////////////////////////////////////////////////////////
// Instantiate the Unit Under Test (UUT_0)
////////////////////////////////////////////////////////////////////////////////

IFAT_AER_connect uut_0(
		.clk(clk), 
		.rst(reset), 

		.ext_IFAT(ext_IFAT), 
		.ext_HIAER(ext_HIAER), 
		.addr(1'b0),

		.if0_din0(if00_din0), 
		.if0_req0(if00_req0), 
		.if0_ack0(if00_ack0), 

		.if0_dout0(if00_dout0), 
		.if0_pulse0(if00_pulse0), 

		.if0_din1(if00_din1), 
		.if0_req1(if00_req1), 
		.if0_ack1(if00_ack1), 

		.if0_dout1(if00_dout1), 
		.if0_pulse1(if00_pulse1), 

		.if0_din2(if00_din2), 
		.if0_req2(if00_req2), 
		.if0_ack2(if00_ack2), 

		.if0_dout2(if00_dout2), 
		.if0_pulse2(if00_pulse2), 

		.if0_din3(if00_din3), 
		.if0_req3(if00_req3), 
		.if0_ack3(if00_ack3), 

		.if0_dout3(if00_dout3), 
		.if0_pulse3(if00_pulse3), 

		.if1_din0(if01_din0), 
		.if1_req0(if01_req0), 
		.if1_ack0(if01_ack0), 

		.if1_dout0(if01_dout0), 
		.if1_pulse0(if01_pulse0), 

		.if1_din1(if01_din1), 
		.if1_req1(if01_req1), 
		.if1_ack1(if01_ack1), 

		.if1_dout1(if01_dout1), 
		.if1_pulse1(if01_pulse1), 

		.if1_din2(if01_din2), 
		.if1_req2(if01_req2), 
		.if1_ack2(if01_ack2), 

		.if1_dout2(if01_dout2), 
		.if1_pulse2(if01_pulse2), 

		.if1_din3(if01_din3), 
		.if1_req3(if01_req3), 
		.if1_ack3(if01_ack3), 

		.if1_dout3(if01_dout3), 
		.if1_pulse3(if01_pulse3), 


		.IFAT_data_out_0(digi0_in0), 
		.IFAT_req_out_0(digi0_inreq0), 
		.ack_IFAT_0(digi0_inack0), 

		.data_IFAT_0(digi0_out0), 
		.request_IFAT_0(digi0_outreq0), 
		.ack_from_IFAT_0(digi0_outack0), 


		.IFAT_data_out_1(digi0_in1), 
		.IFAT_req_out_1(digi0_inreq1), 
		.ack_IFAT_1(digi0_inack1), 

		.data_IFAT_1(digi0_out1), 
		.request_IFAT_1(digi0_outreq1), 
		.ack_from_IFAT_1(digi0_outack1), 


		.ext_in(ext_in), 
		.ext_inreq(ext_inreq), 
		.ext_inack(ext_inack), 

		.ext_out(ext_out), 
		.ext_outreq(ext_outreq),
		.ext_outack(ext_outack) 
);

////////////////////////////////////////////////////////////////////////////////
// Instantiate the Unit Under Test (UUT_1)
////////////////////////////////////////////////////////////////////////////////

IFAT_AER_connect uut_1 (
		.clk(clk), 
		.rst(reset), 

		.ext_IFAT(ext_IFAT), 
		.ext_HIAER(ext_HIAER), 
		.addr(1'b1),

		.if0_din0(if10_din0), 
		.if0_req0(if10_req0), 
		.if0_ack0(if10_ack0), 

		.if0_dout0(if10_dout0), 
		.if0_pulse0(if10_pulse0), 

		.if0_din1(if10_din1), 
		.if0_req1(if10_req1), 
		.if0_ack1(if10_ack1), 

		.if0_dout1(if10_dout1), 
		.if0_pulse1(if10_pulse1), 

		.if0_din2(if10_din2), 
		.if0_req2(if10_req2), 
		.if0_ack2(if10_ack2), 

		.if0_dout2(if10_dout2), 
		.if0_pulse2(if10_pulse2), 

		.if0_din3(if10_din3), 
		.if0_req3(if10_req3), 
		.if0_ack3(if10_ack3), 

		.if0_dout3(if10_dout3), 
		.if0_pulse3(if10_pulse3), 

		.if1_din0(if11_din0), 
		.if1_req0(if11_req0), 
		.if1_ack0(if11_ack0), 

		.if1_dout0(if11_dout0), 
		.if1_pulse0(if11_pulse0), 

		.if1_din1(if11_din1), 
		.if1_req1(if11_req1), 
		.if1_ack1(if11_ack1), 

		.if1_dout1(if11_dout1), 
		.if1_pulse1(if11_pulse1), 

		.if1_din2(if11_din2), 
		.if1_req2(if11_req2), 
		.if1_ack2(if11_ack2), 

		.if1_dout2(if11_dout2), 
		.if1_pulse2(if11_pulse2), 

		.if1_din3(if11_din3), 
		.if1_req3(if11_req3), 
		.if1_ack3(if11_ack3), 

		.if1_dout3(if11_dout3), 
		.if1_pulse3(if11_pulse3), 


		.IFAT_data_out_0(digi1_in0), 
		.IFAT_req_out_0(digi1_inreq0), 
		.ack_IFAT_0(digi1_inack0), 

		.data_IFAT_0(digi1_out0), 
		.request_IFAT_0(digi1_outreq0), 
		.ack_from_IFAT_0(digi1_outack0), 


		.IFAT_data_out_1(digi1_in1), 
		.IFAT_req_out_1(digi1_inreq1), 
		.ack_IFAT_1(digi1_inack1), 

		.data_IFAT_1(digi1_out1), 
		.request_IFAT_1(digi1_outreq1), 
		.ack_from_IFAT_1(digi1_outack1), 


		.ext_in(ext_in), 
		.ext_inreq(ext_inreq), 
		.ext_inack(ext_inack), 

		.ext_out(ext_out), 
		.ext_outreq(ext_outreq),
		.ext_outack(ext_outack) 
);

////////////////////////////////////////////////////////////////////////////////
// Spiking IFAT arrays
////////////////////////////////////////////////////////////////////////////////
IFATarraySim #(72, 12, 14,  0, 11'h000) ifa0(.address(if00_din0), .req(if00_req0), .ack(if00_ack0));
IFATarraySim #(81,  9, 15,  1, 11'h080) ifa1(.address(if00_din1), .req(if00_req1), .ack(if00_ack1));
IFATarraySim #(75, 13, 11,  2, 11'h100) ifa2(.address(if00_din2), .req(if00_req2), .ack(if00_ack2));
IFATarraySim #(62, 20, 21,  3, 11'h180) ifa3(.address(if00_din3), .req(if00_req3), .ack(if00_ack3));
IFATarraySim #(94,  5,  4,  4, 11'h200) ifa4(.address(if01_din0), .req(if01_req0), .ack(if01_ack0));
IFATarraySim #(67, 14, 14,  5, 11'h280) ifa5(.address(if01_din1), .req(if01_req1), .ack(if01_ack1));
IFATarraySim #(70, 23, 24,  6, 11'h300) ifa6(.address(if01_din2), .req(if01_req2), .ack(if01_ack2));
IFATarraySim #(63,  2,  2,  7, 11'h380) ifa7(.address(if01_din3), .req(if01_req3), .ack(if01_ack3));
IFATarraySim #(98, 30, 32,  8, 11'h400) ifa8(.address(if10_din0), .req(if10_req0), .ack(if10_ack0));
IFATarraySim #(91,  7, 13,  9, 11'h480) ifa9(.address(if10_din1), .req(if10_req1), .ack(if10_ack1));
IFATarraySim #(74, 13,  7, 10, 11'h500) ifaA(.address(if10_din2), .req(if10_req2), .ack(if10_ack2));
IFATarraySim #(64,  8,  8, 11, 11'h580) ifaB(.address(if10_din3), .req(if10_req3), .ack(if10_ack3));
IFATarraySim #(95, 10,  5, 12, 11'h600) ifaC(.address(if11_din0), .req(if11_req0), .ack(if11_ack0));
IFATarraySim #(77, 11, 11, 13, 11'h680) ifaD(.address(if11_din1), .req(if11_req1), .ack(if11_ack1));
IFATarraySim #(69, 19, 19, 14, 11'h700) ifaE(.address(if11_din2), .req(if11_req2), .ack(if11_ack2));
IFATarraySim #(76,  1,  1, 15, 11'h780) ifaF(.address(if11_din3), .req(if11_req3), .ack(if11_ack3));

////////////////////////////////////////////////////////////////////////////////
// IFAT interface controller
////////////////////////////////////////////////////////////////////////////////
IFAT_interface fpga(
  .clock(fpgaclk),
  .reset(reset),
  .ext_in(ext_in),
  .ext_inreq(ext_inreq),
  .ext_inack(ext_inack),
  .ext_out(ext_out),
  .ext_outreq(ext_outreq),
  .ext_outack(ext_outack));

////////////////////////////////////////////////////////////////////////////////
// Drive the clocks
////////////////////////////////////////////////////////////////////////////////

always
begin
	#5 clk =  ~clk;
end

always
begin
  #5 fpgaclk = ~fpgaclk;
end

////////////////////////////////////////////////////////////////////////////////
// Initialize reset and clock
////////////////////////////////////////////////////////////////////////////////

initial
begin
  clk = 1'b0;
  fpgaclk = 1'b0;
	reset = 1'b1;
#160;
  reset = 1'b0;
#10000;
  $stop;
end

endmodule
