
`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
//
// Design Name:   top_port
// Project Name:  neovision_test
//
////////////////////////////////////////////////////////////////////////////////

module tb_top_e2e;

// Inputs
reg clk;
reg rst;
reg ext_IFAT;
reg ext_HIAER;
reg addr;
reg [10:0] if0_din0;
reg if0_req0;
reg [10:0] if0_din1;
reg if0_req1;
reg [10:0] if0_din2;
reg if0_req2;
reg [10:0] if0_din3;
reg if0_req3;
reg [10:0] if1_din0;
reg if1_req0;
reg [10:0] if1_din1;
reg if1_req1;
reg [10:0] if1_din2;
reg if1_req2;
reg [10:0] if1_din3;
reg if1_req3;
reg [20:0] digi_in0;
reg digi_req0;
reg digio_ack0;
reg [20:0] digi_in1;
reg digi_req1;
reg digio_ack1;

wire [22:0] ext_in_a;
wire [22:0] ext_in_b;

wire [22:0] ext_out_a;
wire [22:0] ext_out_b;

wire ext_inreq_a;
wire ext_inreq_b;

// wire ext_outack_a;
reg  ext_outack_a;
wire ext_outack_b;

wire ext_inack_a;
wire ext_inack_b;

wire ext_outreq_a;
wire ext_outreq_b;

// Outputs
wire [12:0] if0_dout0;
wire if0_ack0;
wire if0_pulse0;
wire [12:0] if0_dout1;
wire if0_ack1;
wire if0_pulse1;
wire [12:0] if0_dout2;
wire if0_ack2;
wire if0_pulse2;
wire [12:0] if0_dout3;
wire if0_ack3;
wire if0_pulse3;
wire [12:0] if1_dout0;
wire if1_ack0;
wire if1_pulse0;
wire [12:0] if1_dout1;
wire if1_ack1;
wire if1_pulse1;
wire [12:0] if1_dout2;
wire if1_ack2;
wire if1_pulse2;
wire [12:0] if1_dout3;
wire if1_ack3;
wire if1_pulse3;
wire digi_ack0;
wire [12:0] digi_out0;
wire digio_req0;
wire digi_ack1;
wire [12:0] digi_out1;
wire digio_req1;

integer i1, i2, i3, i4;

////////////////////////////////////////////////////////////////////////////////
// Instantiate the FPGA
////////////////////////////////////////////////////////////////////////////////

main u_fpga (
           .sysclk_p(clk),
           .sysclk_n(),
           .rst(rst),

           .DATA_USB(),
           .RXF_B(1'b1),
           .TXE_B(1'b1),
           .RD_B(),
           .WR(),

           .CS_B_LD(),
           .SCK(),
           .SDI(),

           .EXT_HIAER_B(),
           .EXT_IFAT_B(),

           .IFAT_clk(clk_ifat),
           .RST_B(),
           .RESETLFSR_B(),
           .RESETIFAT_B(reset_ifat),

           .EXT_IN_TOP(ext_in_b),
           .EXT_IN_BOTTOM(ext_in_a),
           .EXT_OUT_TOP(ext_out_b),
           .EXT_OUT_BOTTOM(ext_out_a),

           .EXT_INREQ({ext_inreq_a, ext_inreq_b}),
           .EXT_INACK({ext_inack_a, ext_inack_b}),
           .EXT_OUTREQ({ext_outreq_a, ext_outreq_b}),
//         .EXT_OUTACK({ext_outack_a, ext_outack_b}), // to be restored after functionality is understood
           .EXT_OUTACK(),

           .LED_OFF()
);

////////////////////////////////////////////////////////////////////////////////
// Instantiate the Unit Under Test (UUT_A)
////////////////////////////////////////////////////////////////////////////////

IFAT_AER_connect uut_a (
//		.clk(clk_ifat),    // to be restored after functionality is understood
//		.rst(reset_ifat),  // to be restored after functionality is understood
		.clk(clk), 
		.rst(rst), 

		.ext_IFAT(ext_IFAT), 
		.ext_HIAER(ext_HIAER), 
		.addr(addr),

		.if0_din0(if0_din0), 
		.if0_dout0(if0_dout0), 
		.if0_req0(if0_req0), 
		.if0_ack0(if0_ack0), 
		.if0_pulse0(if0_pulse0), 
		.if0_din1(if0_din1), 
		.if0_dout1(if0_dout1), 
		.if0_req1(if0_req1), 
		.if0_ack1(if0_ack1), 
		.if0_pulse1(if0_pulse1), 
		.if0_din2(if0_din2), 
		.if0_dout2(if0_dout2), 
		.if0_req2(if0_req2), 
		.if0_ack2(if0_ack2), 
		.if0_pulse2(if0_pulse2), 
		.if0_din3(if0_din3), 
		.if0_dout3(if0_dout3), 
		.if0_req3(if0_req3), 
		.if0_ack3(if0_ack3), 
		.if0_pulse3(if0_pulse3), 

		.if1_din0(if1_din0), 
		.if1_dout0(if1_dout0), 
		.if1_req0(if1_req0), 
		.if1_ack0(if1_ack0), 
		.if1_pulse0(if1_pulse0), 
		.if1_din1(if1_din1), 
		.if1_dout1(if1_dout1), 
		.if1_req1(if1_req1), 
		.if1_ack1(if1_ack1), 
		.if1_pulse1(if1_pulse1), 
		.if1_din2(if1_din2), 
		.if1_dout2(if1_dout2), 
		.if1_req2(if1_req2), 
		.if1_ack2(if1_ack2), 
		.if1_pulse2(if1_pulse2), 
		.if1_din3(if1_din3), 
		.if1_dout3(if1_dout3), 
		.if1_req3(if1_req3), 
		.if1_ack3(if1_ack3), 
		.if1_pulse3(if1_pulse3), 

		.IFAT_data_out_0(21'd0), 
		.IFAT_req_out_0(1'b0), 
		.ack_IFAT_0(), 
		.data_IFAT_0(), 
		.request_IFAT_0(), 
		.ack_from_IFAT_0(1'b0), 

		.IFAT_data_out_1(21'd0), 
		.IFAT_req_out_1(1'b0), 
		.ack_IFAT_1(), 
		.data_IFAT_1(), 
		.request_IFAT_1(), 
		.ack_from_IFAT_1(1'b0), 

		.ext_in(ext_in_a), 
		.ext_inack(ext_inack_a), 
		.ext_inreq(ext_inreq_a), 
		.ext_out(ext_out_a), 
		.ext_outack(ext_outack_a), 
		.ext_outreq(ext_outreq_a)
);

////////////////////////////////////////////////////////////////////////////////
// Instantiate the Unit Under Test (UUT_B)
////////////////////////////////////////////////////////////////////////////////

IFAT_AER_connect uut_b (
		.clk(clk_ifat), 
		.rst(reset_ifat), 

		.ext_IFAT(ext_IFAT), 
		.ext_HIAER(ext_HIAER), 
		.addr(addr),

		.if0_din0(11'd0), 
		.if0_dout0(), 
		.if0_req0(1'b0), 
		.if0_ack0(), 
		.if0_pulse0(), 
		.if0_din1(11'd0), 
		.if0_dout1(), 
		.if0_req1(1'b0), 
		.if0_ack1(), 
		.if0_pulse1(), 
		.if0_din2(11'd0), 
		.if0_dout2(), 
		.if0_req2(1'b0), 
		.if0_ack2(), 
		.if0_pulse2(), 
		.if0_din3(11'd0), 
		.if0_dout3(), 
		.if0_req3(1'b0), 
		.if0_ack3(), 
		.if0_pulse3(), 

		.if1_din0(11'd0), 
		.if1_dout0(), 
		.if1_req0(1'b0), 
		.if1_ack0(), 
		.if1_pulse0(), 
		.if1_din1(11'd0), 
		.if1_dout1(), 
		.if1_req1(1'b0), 
		.if1_ack1(), 
		.if1_pulse1(), 
		.if1_din2(11'd0), 
		.if1_dout2(), 
		.if1_req2(1'b0), 
		.if1_ack2(), 
		.if1_pulse2(), 
		.if1_din3(11'd0), 
		.if1_dout3(), 
		.if1_req3(1'b0), 
		.if1_ack3(), 
		.if1_pulse3(), 

		.IFAT_data_out_0(21'd0), 
		.IFAT_req_out_0(1'b0), 
		.ack_IFAT_0(), 
		.data_IFAT_0(), 
		.request_IFAT_0(), 
		.ack_from_IFAT_0(1'b0), 

		.IFAT_data_out_1(21'd0), 
		.IFAT_req_out_1(1'b0), 
		.ack_IFAT_1(), 
		.data_IFAT_1(), 
		.request_IFAT_1(), 
		.ack_from_IFAT_1(1'b0), 

		.ext_in(ext_in_b), 
		.ext_inack(ext_inack_b), 
		.ext_inreq(ext_inreq_b), 
		.ext_out(ext_out_b), 
		.ext_outack(ext_outack_b), 
		.ext_outreq(ext_outreq_b)
);

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Input initialization
////////////////////////////////////////////////////////////////////////////////

initial begin
	// Initialize Inputs
        clk = 1'b0;

	ext_IFAT = 0;
	ext_HIAER = 0;

	addr = 1'b0;

	if0_din0 = 0;
//	if0_req0 = 0;
	if0_din1 = 0;
//	if0_req1 = 0;
	if0_din2 = 0;
//	if0_req2 = 0;
	if0_din3 = 0;
//	if0_req3 = 0;

	if1_din0 = 0;
	if1_req0 = 0;
	if1_din1 = 0;
	if1_req1 = 0;
	if1_din2 = 0;
	if1_req2 = 0;
	if1_din3 = 0;
	if1_req3 = 0;

	digi_in0 = 0;
	digi_req0 = 0;
	digio_ack0 = 0;
	digi_in1 = 0;
	digi_req1 = 0;
	digio_ack1 = 0;
end

////////////////////////////////////////////////////////////////////////////////
// Initialize reset and clock
////////////////////////////////////////////////////////////////////////////////

initial
begin	
	rst = 1'b1;
#160;
  rst = 1'b0;
end


////////////////////////////////////////////////////////////////////////////////
// Drive the clock
////////////////////////////////////////////////////////////////////////////////

always
begin
	#10 clk =  ~clk;
end

////////////////////////////////////////////////////////////////////////////////
// Setup configuration for test
////////////////////////////////////////////////////////////////////////////////

initial
begin

////////////////////////////////////////////////////
// configure in ext_IFAT mode
////////////////////////////////////////////////////

	ext_IFAT <= 1'b0;
	ext_HIAER <=1'b0;
#60;
	ext_IFAT <= 1'b1;
	ext_HIAER <=1'b0;

end

////////////////////////////////////////////////////////////////////////////////
// Stimuli from IFAT array
////////////////////////////////////////////////////////////////////////////////

initial
begin

	 if0_req0<= 1'b0;

        for (i1=0;i1<=4;i1=i1+1)
        begin
	 if0_req0<= 1'b1;
        @(posedge if0_ack0)
         #71;
	 if0_req0<= 1'b0;
        #200;
        end

        for (i1=0;i1<=50;i1=i1+1)
        begin
	 if0_req0<= 1'b1;
        @(posedge if0_ack0)
         #74;
	 if0_req0<= 1'b0;
        #200;
        end

end		

initial
begin

	 if0_req1<= 1'b0;

        for (i2=0;i2<=4;i2=i2+1)
        begin
	 if0_req1<= 1'b1;
        @(posedge if0_ack1)
         #27;
	 if0_req1<= 1'b0;
        #200;
        end

        #3;
        for (i2=0;i2<=50;i2=i2+1)
        begin
	 if0_req1<= 1'b1;
        @(posedge if0_ack1)
         #29;
	 if0_req1<= 1'b0;
        #200;
        end

end		

initial
begin

	 if0_req2<= 1'b0;

        for (i3=0;i3<=4;i3=i3+1)
        begin
	 if0_req2<= 1'b1;
        @(posedge if0_ack2)
         #42;
	 if0_req2<= 1'b0;
        #200;
        end

        #6;
        for (i3=0;i3<=50;i3=i3+1)
        begin
	 if0_req2<= 1'b1;
        @(posedge if0_ack2)
         #45;
	 if0_req2<= 1'b0;
        #200;
        end

end		

initial
begin

	 if0_req3<= 1'b0;

        for (i4=0;i4<=4;i4=i4+1)
        begin
	 if0_req3<= 1'b1;
        @(posedge if0_ack3)
         #68;
	 if0_req3<= 1'b0;
        #200;
        end

        #9;
        for (i4=0;i4<=50;i4=i4+1)
        begin
	 if0_req3<= 1'b1;
        @(posedge if0_ack3)
         #63;
	 if0_req3<= 1'b0;
        #200;
        end

$stop;

end		

////////////////////////////////////////////////////////////////////////////////
// Stimuli from FPGA
////////////////////////////////////////////////////////////////////////////////

always
  begin
      ext_outack_a <= 1'b0;
    @(posedge ext_outreq_a )
      #26;
      ext_outack_a <= 1'b1;
    @(negedge ext_outreq_a)
      #37;
      ext_outack_a <= 1'b0;
  end

endmodule
