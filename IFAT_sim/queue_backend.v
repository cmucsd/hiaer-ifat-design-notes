`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
// 
// Create Date:    23:46:12 02/09/2013 
// Design Name: 
// Module Name:    queue_backend 
// Design Name:    HiAER-IFAT v.1.0
// Target Devices: 
// Tool versions: 
// Description: module refactored from Siddharth's synthesized IFAT analog logic
//              (should be functionally equivalent)
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module queue_backend(
  clk,
  rst, 
  op_req,
  address_event,
  pop_req,
  if_out_al,
  if_en
  );

input clk;    // master clock
input rst;    // master reset
input op_req; // request output from priority FIFO
input [21:0] address_event;
output pop_req; // request to pop address event from priority FIFO
output [12:0] if_out_al;  // synapse address
output if_en;       // enable synaptic pulse to analog neurons

reg pop_req;  // request datum from queue
reg [12:0] if_out_al;
reg if_en;    // output pulse enable

reg [1:0] sr, nst;  // state, next state
reg [5:0] counter;  // PWM count
reg dcnt;     // down counter enable
reg en_out;

wire [5:0] weight; // 6-bit synaptic weight (to preload counter)
wire [12:0] synapse_address;
assign weight = address_event[5:0];
assign synapse = address_event[18:6];

parameter 
  reset_state = 2'b00, 
  idle_state = 2'b01, 
  count_state = 2'b10, 
  state3 = 2'b11;

// state update
always @(posedge clk, posedge rst)
begin
  if (rst == 1'b1)
    begin
      sr <= reset_state;
    end
  else
    begin
      sr <= nst;
    end
end

// output queue to IFAT synaptic input controller
always @(sr, op_req, counter)
begin
  case (sr)
  reset_state: 
    begin
      en_out <= 1'b0;
      pop_req <= 1'b0;
      nst <= idle_state;
      dcnt <= 1'b0;
    end
  idle_state: 
    begin
      dcnt <= 1'b0;
      if (op_req == 1'b1)
        begin
          en_out <= 1'b1;
          pop_req <= 1'b1;
          nst <= count_state;
        end
      else
        begin
          en_out <= 1'b0;
          pop_req <= 1'b0;
          nst <= idle_state;
        end
    end
  count_state: 
    begin
      en_out <= 1'b0;
      pop_req <= 1'b0;
      if (counter > 6'b0)
        begin
          dcnt <= 1'b1;
          nst <= count_state;
        end
      else
        begin
          dcnt <= 1'b0;
          nst <= idle_state;
        end
    end
  default:
    begin
      en_out <= 1'b0;
      pop_req <= 1'b0;
      dcnt <= 1'b0;
      nst <= idle_state;
    end
  endcase
end

// synaptic pulse generator
always @(posedge clk, posedge rst)
begin
  if (rst == 1'b1)
    begin
      counter <= 6'b0;
      if_en <= 1'b0;
    end
  else
    begin
    if (pop_req == 1'b1)  // at pop request
      begin
        counter <= weight;  // load synaptic weight into counter
        if_en <= 1'b0;
      end
    else
      if (dcnt == 1'b1) // count enabled
        begin
          counter <= counter - 1'b1;
          if_en <= 1'b1;  // apply synaptic pulse
        end
      else
        begin
          counter <= counter;
          if_en <= 1'b0;
        end
    end
end

// synapse address extraction
always @ (posedge clk, posedge rst)
begin
	if (rst == 1'b1)
    begin
      if_out_al <= 13'b0;
    end
 	else
    begin
      if (pop_req == 1'b1)
        if_out_al <= synapse;
      else
        if_out_al <= if_out_al;
    end
end

endmodule
