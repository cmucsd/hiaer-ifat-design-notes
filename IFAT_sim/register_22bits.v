`timescale 1ns/1ps

module register_22bits(out_data, out_data1, in_data, read_enable, write_enable);
output [21:0] out_data;
output [21:0] out_data1;

input [21:0] in_data;
input read_enable;
input write_enable;

//wire read_enable_b;

//not I0(read_enable_b, read_enable);

RF2R1WX1TL RF00(.R1B(out_data[0]), .R2B(out_data1[0]), .WB(in_data[0]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF01(.R1B(out_data[1]), .R2B(out_data1[1]), .WB(in_data[1]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF02(.R1B(out_data[2]), .R2B(out_data1[2]), .WB(in_data[2]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF03(.R1B(out_data[3]), .R2B(out_data1[3]), .WB(in_data[3]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF04(.R1B(out_data[4]), .R2B(out_data1[4]), .WB(in_data[4]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));

RF2R1WX1TL RF05(.R1B(out_data[5]), .R2B(out_data1[5]), .WB(in_data[5]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF06(.R1B(out_data[6]), .R2B(out_data1[6]), .WB(in_data[6]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF07(.R1B(out_data[7]), .R2B(out_data1[7]), .WB(in_data[7]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF08(.R1B(out_data[8]), .R2B(out_data1[8]), .WB(in_data[8]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF09(.R1B(out_data[9]), .R2B(out_data1[9]), .WB(in_data[9]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));

RF2R1WX1TL RF10(.R1B(out_data[10]), .R2B(out_data1[10]), .WB(in_data[10]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF11(.R1B(out_data[11]), .R2B(out_data1[11]), .WB(in_data[11]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF12(.R1B(out_data[12]), .R2B(out_data1[12]), .WB(in_data[12]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF13(.R1B(out_data[13]), .R2B(out_data1[13]), .WB(in_data[13]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF14(.R1B(out_data[14]), .R2B(out_data1[14]), .WB(in_data[14]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));

RF2R1WX1TL RF15(.R1B(out_data[15]), .R2B(out_data1[15]), .WB(in_data[15]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF16(.R1B(out_data[16]), .R2B(out_data1[16]), .WB(in_data[16]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF17(.R1B(out_data[17]), .R2B(out_data1[17]), .WB(in_data[17]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF18(.R1B(out_data[18]), .R2B(out_data1[18]), .WB(in_data[18]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF19(.R1B(out_data[19]), .R2B(out_data1[19]), .WB(in_data[19]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));

RF2R1WX1TL RF20(.R1B(out_data[20]), .R2B(out_data1[20]), .WB(in_data[20]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));
RF2R1WX1TL RF21(.R1B(out_data[21]), .R2B(out_data1[21]), .WB(in_data[21]), .WW(write_enable), .R1W(read_enable), .R2W(read_enable));

endmodule



