`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:41:36 02/19/2013 
// Design Name: 
// Module Name:    IFAT_interface 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Module containing the interface to the IFAT chip 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IFAT_interface(
  input clock,
  input reset,
// events into the synapses
  output [22:0] ext_in,
  output ext_inreq,
  input ext_inack,
// events from the neurons
  input [22:0] ext_out,
  input ext_outreq,
  output reg ext_outack
  );

wire [31:0] aer;
assign ext_in = aer[22:0];

// AER stream generator
AERstreamSim #(161, 5, 5, 0, -1) 
IncomingStreamGenerator(
  .AddressEvent(aer), 
  .request(ext_inreq), 
  .acknowledge(ext_inack), 
  .clock(clock), 
  .reset(reset)
);

initial
begin
  ext_outack <= 1'b0;
end

always @(posedge clock)
begin
  if (reset)
  begin
    ext_outack <= 1'b0;
  end
  else
  begin
    ext_outack <= ext_outreq;
  end
end

always @(posedge ext_outack)
begin
  $display("Time %d: Address event %h received", $time, ext_out);
end

endmodule
