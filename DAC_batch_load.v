`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:58:02 10/27/2011 
// Design Name: 
// Module Name:    DAC_batch_load 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DAC_batch_load(
// general input
	input	clk,					// subdivided sysclk (@ 20MHz) from main loop
	input	rst,

	input	DAC_req,				// FPGA generated request to load DAC values

	input	[159:0] DAC_DATA_BATCH,	// data array for 5x32bit daisychain dacs
	
	output	CS_B_LD,				// dac control signals
    output	SCK,
    output	SDI,
	output	DAC_done				// output ack signal to indicate done
    );
// DAC signals
    wire    CS_B_LD;
    wire    SCK;
    wire    SDI;
	wire	DAC_done;
     
// local variables    
	wire	[159:0] DAC_DATA_BATCH;
	reg		[159:0]	DAC_DATA;

	reg		[7:0] dacindex;
    reg     [2:0] state;
	
    parameter
        RESET       	= 3'h0,
        WAIT        	= 3'h1,
        WRITE_B     	= 3'h2,
        WRITE       	= 3'h3,
        LATCH       	= 3'h4,
        LATCH_B     	= 3'h5;
		
// define DAC output combinatorial signals
	assign 	CS_B_LD = (state == RESET || state == WAIT || 
						state == LATCH || state == LATCH_B);
	assign	SCK = (state == WRITE);
	assign  SDI = (state == WRITE || state == WRITE_B)? DAC_DATA[159] : 1'b0;
	assign	DAC_done = (state == LATCH || state == LATCH_B);

// define clocked state transitions
    always @(posedge clk or posedge rst)
	begin
		if (rst)
			state <= RESET;
        else
        begin
            case (state)
            RESET : 
                begin
                    state <= WAIT;
                end
            WAIT :
                begin
					dacindex <= 8'h00;
					if (DAC_req == 1'b1)					// wait for request to write DAC values
						begin
							DAC_DATA <= DAC_DATA_BATCH;
							state <= WRITE_B;
						end
					else
						state <= WAIT;
                end

            WRITE_B :
                begin
                    dacindex <= dacindex + 8'd1;			// update dac data index	
                    state <= WRITE;
                end
            WRITE :
                begin
                    DAC_DATA <= {DAC_DATA[158:0], 1'b0};	// shift through dac data
                    state <= (dacindex > 8'd159)? LATCH : WRITE_B;
                end

// latch DAC data values onto DAC(s)				
            LATCH :
                begin
                    state <= LATCH_B;
                end
            LATCH_B :
				begin
					state <= WAIT;
				end
				
			default :
				begin
					state <= WAIT;
				end
			endcase
		end
	end
endmodule
