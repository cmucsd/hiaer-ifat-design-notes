`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
//
// Create Date:   18:53:23 08/15/2012
// Design Name:   data_read
// Module Name:   C:/Xilinx/projects/IFAT_out_fifo_test/test_data_read.v
// Project Name:  dac
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: data_read
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_data_read;

	// Inputs
	reg clk;
	reg request;
	reg data_valid;

	// Outputs
	wire address_strobe;
	wire parallel_load;
	wire acknowledge;

	// Instantiate the Unit Under Test (UUT)
	data_read uut (
		.clk(clk), 
		.request(request), 
		.data_valid(data_valid), 
		.address_strobe(address_strobe), 
		.parallel_load(parallel_load), 
		.acknowledge(acknowledge)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		request = 0;
		data_valid = 0;
		// Add stimulus here
        #21 request= 1;
        #5 request= 0;
        #12.4 data_valid= 1;
        #5 data_valid= 0;
        #41 request= 1;
        #6 data_valid= 1;
        #5 data_valid= 0;
        #30.1 request= 0;
	end
      
    always
        #2.5 clk= ~clk;
      
endmodule

