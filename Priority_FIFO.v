//
//
//  Priority FIFO
//  Priority_FIFO.v
//
//  Jongkil Park
//
//  This is for FPGA version Priority FIFO
//  There is room for reducing state change.
//  
//
`timescale 1ns/1ps

module Priority_FIFO #(
parameter WIDTH = 33,
parameter DEPTH = 512,
parameter ADDR_PNTR = 9,
parameter TIMER_WIDTH = 10
)
(
//general input
input clk,
input rst,

input [WIDTH - 2:0] data_in,
input req_previous_this,

//input
input [TIMER_WIDTH - 1:0] global_time, 
input ack_next_this,

//output
output [21:0] data_out_alias, 
output [9:0] data_out_time_stamp, 
output reg req_this_next, 
output reg ack_this_previous, 

output buffer_full, //to notice buffer status to other block
output reg buffer_empty);


/////////////////////////
//output relate register
/////////////////////////
reg [WIDTH - 2:0] data_out;


assign data_out_alias = data_out[31:10]; //we just need 22bits to indicate DRAM address
assign data_out_time_stamp = data_out[9:0];

/////////////////////////
//internal register
/////////////////////////
reg [ADDR_PNTR:0] count_fifo; // To count number of entry in this priority queue
reg [ADDR_PNTR:0] count_reg; //

reg [TIMER_WIDTH - 1:0] minimum_delay, earliest_time; // Current minimum_delay and earliest time

reg [TIMER_WIDTH - 1:0] pre_global_time;
reg [ADDR_PNTR - 1:0] data_address; //address line of memory generator

////////////////////////////
// pointer control
//
reg [ADDR_PNTR - 1:0] wr_pt;
reg [ADDR_PNTR - 1:0] rd_pt;
reg [ADDR_PNTR - 1:0] next_rd_pt;
reg [ADDR_PNTR - 1:0] comp_rd_pt;

reg wr_pt_up;
reg rd_pt_up;
reg same_pt;
reg next_pop;

///////////////////////
// 
reg [1:0] wr_state;
reg compare;
reg pre_comp;
reg comp;
reg comp_del;

reg enable_write;
reg enable_read;
reg enable_col;

//reg read_latency;

//data related
wire [WIDTH - 1:0] data_wire;
reg  same_time;
reg [WIDTH - 1:0] data_buf;

//delay
reg [TIMER_WIDTH - 1:0] delay_com; //data_out - global_time
reg [TIMER_WIDTH - 1:0] delay_wr;  //data_in - global_time

reg [TIMER_WIDTH - 1:0] ear_time;

reg diff_global_time; //pre_global_time != global_time
reg ear_global_time; //earliest time == global_time

//state

reg data_av;

reg av_update;


reg write_pt_up;
reg find_write;

reg read;
reg write;

reg comp_write_pt_up;
reg valid;
reg write_pop;

reg comp_done;

wire delay_past;
wire full_add_cnt;
wire empty_add_cnt;

reg next_rd_pt_write;
reg next_rd_pt_read;
reg write_pointer_loading;


////////////////////
// accept request signal
//

reg data_buf_take;

always @(posedge clk or posedge rst)
	if(rst)
		data_buf <= 0;
	else if(av_update)
		data_buf <= {1'b0, data_out};
	else if(data_buf_take)
		data_buf <= {1'b1, data_in};



////////////////////
// pre_global_time
// global_time compare
always @(posedge clk or posedge rst)
	if(rst)
		pre_global_time <=  0; //10bits
	else
		pre_global_time <=  global_time;

always @(posedge clk or posedge rst)
	if(rst)
		diff_global_time <= 1'b0;
	else if(pre_global_time == global_time)
		diff_global_time <= 1'b0;
	else
		diff_global_time <= 1'b1;

always @(posedge clk or posedge rst)
	if(rst)
		ear_global_time <= 1'b0;
	else if(pre_comp)
		ear_global_time <= 1'b0;
	else if(global_time == earliest_time && valid && diff_global_time)
		ear_global_time <= 1'b1;
	//else
	//	ear_global_time <= 1'b0;

////////////////////
// minimum_delay
// earliest_time
always @(posedge clk or posedge rst)
	if(rst)
		earliest_time <=  0; //10bits
	else if(wr_state == 2'b01)
		earliest_time <=  ear_time;
	else if(wr_state == 2'b10)
		earliest_time <=  data_out[TIMER_WIDTH - 1:0];

always @(posedge clk or posedge rst)
	if(rst)
		minimum_delay <= 0;  //10bits
	else if(diff_global_time && valid)
		minimum_delay <= minimum_delay - 1'b1;
	else if(wr_state == 2'b01)
		minimum_delay <= delay_wr[TIMER_WIDTH - 1:0];
	else if(wr_state == 2'b10)
		minimum_delay <= delay_com[TIMER_WIDTH - 1:0];



always @(posedge clk or posedge rst)
	if(rst)
		valid <= 1'b0;
	else if(wr_state == 2'b01 || (wr_state == 2'b10 && data_av))
		valid <= 1'b1;
//	else if(av_update && !write_pop)
	else if(av_update && !same_time)
		valid <= 1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		write_pop <= 1'b0;
	else if(same_pt)
		write_pop <= 1'b1;
	else if(av_update)
		write_pop <= 1'b0;


always @(posedge clk or posedge rst)
	if(rst)
		count_reg <=  DEPTH;
	else if(compare)
		count_reg <=  count_reg + 1'b1;
	else if(pre_comp)
		count_reg <=  0;



always @(posedge clk or posedge rst)
	if(rst)
		comp_done <= 1'b1;
	else if(pre_comp)
		comp_done <= 1'b0;
	else if(count_reg == (DEPTH - 1) && compare)
		comp_done <= 1'b1;
	else if(count_reg == DEPTH )
		comp_done <= 1'b1;
	else
		comp_done <= 1'b0;



always @(posedge clk or posedge rst)
	if(rst)
		same_time <= 1'b0; //global time is same as comparison value
	else if((data_out[TIMER_WIDTH - 1:0] == global_time) && data_av)
		same_time <= 1'b1;
	else
		same_time <= 1'b0;
	

///////////////////////
// delay calculation
//


reg comp_del_done;
reg comp_wr_done;
reg comp_del_start;
reg comp_wr_start;

always @(posedge clk or posedge rst)
	if(rst)
		delay_com <=  0;  //10bits
	else if(rd_pt_up)
		delay_com <= data_out[TIMER_WIDTH - 1:0] - global_time; //data_out[9:0] - global_time

always @(posedge clk or posedge rst)
	if(rst)
		comp_del <= 1'b0;
	else if(comp_del_done)
		comp_del <= 1'b0;
	else if(comp_del_start && (minimum_delay > delay_com) && valid || minimum_delay == 0)
		comp_del <= 1'b1;
	else if(comp_del_start && !valid)
		comp_del <= 1'b1;
//	else if((minimum_delay > delay_com) && valid)//minimum_delay > delay_com
//		comp_del <= 1'b1;
//	else if(!valid)
//		comp_del <= 1'b1;
//	else
//		comp_del <= 1'b0;		

always @(posedge clk or posedge rst)
	if(rst)
	begin
		delay_wr <=  0; //10bits
		ear_time <=  0; //10bits
	end
	else if(req_previous_this)
	begin
		delay_wr <=  data_in[TIMER_WIDTH - 1:0] - global_time; //data_buf[9:0] - global_time
		ear_time <= data_in[TIMER_WIDTH - 1:0];
	end
		
always @(posedge clk or posedge rst)
	if(rst)
		comp <= 1'b0;
	else if(comp_wr_start && (minimum_delay > delay_wr) && valid)
		comp <= 1'b1;
	else if(comp_wr_start && !valid)
		comp <= 1'b1;
	else if(comp_wr_done)
		comp <= 1'b0;
	//else if((minimum_delay > delay_wr) && valid)//minimum_Delay > delay_wr
	//	comp <= 1'b1;
	//else if(!valid)
	//	comp <= 1'b1;
	//else
	//	comp <= 1'b0;

/////////////////////
// count_fifo
//
// In Priority FIFO, write and read can not be happened simultaniously
always @(posedge clk or posedge rst)
	if(rst)
		count_fifo <=  0;
	else if(ack_this_previous)
		count_fifo <=  count_fifo + 1'b1;
	else if(av_update)
		count_fifo <=  count_fifo - 1'b1;

////////////////////
// pointer control
//

always @(posedge clk or posedge rst)
  if(rst)
    data_address <= 0;
  else if(write_pointer_loading)
    data_address <= wr_pt;
  else if(read)
    data_address <= rd_pt;

/////////////////////
// write address control
//

reg [ADDR_PNTR - 1:0] din;
reg we, re;
wire [ADDR_PNTR - 1:0] dout;
wire [2:0] add_cnt;
reg [ADDR_PNTR - 1:0] write_pt;


reg [ADDR_PNTR - 1:0] rd_pt_memory;

always @(posedge clk or posedge rst)
	if(rst)
		wr_pt <= 0;
	else if(wr_pt_up)
		wr_pt <= dout;
	else if(av_update )
		wr_pt <= rd_pt_memory;
		
always @(posedge clk or posedge rst)
	if(rst)
		rd_pt_memory <= 0;
	else if(enable_read)
		rd_pt_memory <= rd_pt;


always @(posedge clk or posedge rst)
	if(rst)
		re <= 1'b0;
	else if(wr_pt_up)
		re <= 1'b1;
	else
		re <= 1'b0;
		

always @(posedge clk or posedge rst)
	if(rst)
	begin
		we <= 1'b0;
		din <= 0;
	end
	else if(write_pt_up) //available
	begin
		we <= 1'b1;
		din <= write_pt;
	end
	else
	begin
		we <= 1'b0;
		din <= din;
	end

always @(posedge clk or posedge rst)
	if(rst)
		write_pt <= 4;
	else if(comp_write_pt_up)
		write_pt <= write_pt + 9'b1;


address_FIFO add_fifo(.clk(clk), .rst(rst), .din(din), .we(we), .dout(dout), .re(re), .count_fifo(add_cnt));


///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

		
always @(posedge clk or posedge rst)
	if(rst)
		rd_pt <= 0;
	else if(rd_pt_up) //compare purpose
		rd_pt <= comp_rd_pt + 9'b1;
	else if(same_pt)
		rd_pt <= wr_pt;
	else if(next_pop)
		rd_pt <= next_rd_pt;
	else if(find_write)
		rd_pt <= write_pt;

always @(posedge clk or posedge rst)
	if(rst)
		comp_rd_pt <= 0;
	else if(rd_pt_up)
		comp_rd_pt <= comp_rd_pt + 9'b1;
	else if(next_pop)
		comp_rd_pt <= next_rd_pt;



always @(posedge clk or posedge rst)
	if(rst)
		data_av <= 1'b0;
	else if(enable_col)
		data_av <= data_wire[32];
	else if(av_update)
		data_av <= 0;
		

always @(posedge clk or posedge rst)
	if(rst)
		enable_read <= 1'b0;
	else if(read)
		enable_read <= 1'b1;
	else if(enable_col)
		enable_read <= 1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		enable_col <= 1'b0;
	else if(enable_read)
		enable_col <= 1'b1;
	else
		enable_col <= 1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		enable_write <= 1'b1;
	else if(write)
		enable_write <= 1'b1;
	else
		enable_write <= 1'b0;

//////////////////////////////////////
//////////////////////////////////////

always @(posedge clk or posedge rst)
	if(rst)
		buffer_empty <= 1'b1;
	else if(count_fifo != 0)
		buffer_empty <= 1'b0;
	else if(count_fifo == 0)
		buffer_empty <= 1'b1;
		
assign buffer_full = count_fifo[ADDR_PNTR];


always @(posedge clk or posedge rst)
	if(rst)
		next_rd_pt <= 0;
	else if(next_rd_pt_write)
		next_rd_pt <= wr_pt;
	else if(next_rd_pt_read)
		next_rd_pt <= rd_pt - 9'b1;


assign full_add_cnt = (add_cnt == 3'b100); 
assign empty_add_cnt = (add_cnt == 3'b000);

wire delay_same;
assign delay_past = delay_wr[9];
assign delay_same = delay_wr == 0;

parameter [4:0] RESET =	5'b00000;
parameter [4:0] IDLE = 	5'b00001;
parameter [4:0] WRITE = 5'b00010;
parameter [4:0] WRITE1 = 5'b00100;
parameter [4:0] WRITE_AFTER = 5'b00101;
parameter [4:0] COMPARE = 5'b00110;
parameter [4:0] COMPARE_NEXT_PRE = 5'b00111;
parameter [4:0] COMPARE_NEXT = 5'b01000;
parameter [4:0] COMPARE_NEXT1 = 5'b01001;
parameter [4:0] COMPARE_NEXT2 = 5'b01010;
parameter [4:0] POP = 5'b01011;
parameter [4:0] POP_READ_LATENCY1 = 5'b01100;
parameter [4:0] POP_READ_LATENCY2 = 5'b01101;
parameter [4:0] POP1 = 5'b01110;
parameter [4:0] POP_WAIT = 5'b01111;
parameter [4:0] FIND_WRITE_LOC = 5'b10000;
parameter [4:0] FIND_WRITE_LOC1 = 5'b10001;
parameter [4:0] FIND_WRITE_LOC2 = 5'b10010;
parameter [4:0] FIND_WRITE_LOC3 = 5'b10011;
parameter [4:0] WRITE_PRECHARGE = 5'b10100;
parameter [4:0] WRITE_POINTER_LOADING = 5'b10101;
parameter [4:0] COMPARE_NEXT0 = 5'b10110;
parameter [4:0] COMPARE_NEXT01 = 5'b10111;
parameter [4:0] COMPARE_NEXT02 = 5'b11000;

	
reg [4:0]  current_state, next_state;
wire request_avail;

assign request_avail = req_previous_this && !empty_add_cnt && !buffer_full;

///////////////////////
// test purpose register
//
reg rq_av;

always @(posedge clk or posedge rst)
if(rst)
	rq_av <= 0;
else if(request_avail)
	rq_av <= 1;
else
	rq_av <= 0;
////////////////////
//	Current State Register
//
always @(posedge clk or posedge rst)
	if(rst)
		current_state <= RESET;
	else
		current_state <=  next_state;

////////////////////
//	State Machine
//
always @(*)
begin

	next_state = current_state;
	wr_pt_up =  1'b0;
	rd_pt_up =  1'b0;

	same_pt =  1'b0;

	next_rd_pt_write = 1'b0;
	next_rd_pt_read = 1'b0;

	next_pop =  1'b0;

	write =  1'b0; 
	read =  1'b0;

	req_this_next =  1'b0;

	ack_this_previous =  1'b0;
				
	wr_state =  2'b00;
				
	//to calculate count_reg
	compare =  1'b0;
	pre_comp =  1'b0;

	av_update = 1'b0;
	

	find_write = 1'b0;
	write_pt_up = 1'b0;

	comp_write_pt_up = 1'b0;
	write_pointer_loading = 1'b0;
	
	comp_wr_done = 1'b0;
	comp_del_done = 1'b0;
	
	comp_del_start = 1'b0;
	comp_wr_start = 1'b0;

	data_buf_take = 1'b0;
	
	case(current_state)
		RESET:
		begin
			wr_pt_up =  1'b0;
			rd_pt_up =  1'b0;

			same_pt =  1'b0;

			next_rd_pt_write = 1'b0;
			next_rd_pt_read = 1'b0;

			next_pop =  1'b0;

			write =  1'b0; 
			read =  1'b0;

			req_this_next =  1'b0;

			ack_this_previous =  1'b0;
				
			wr_state =  2'b00;
				
			//to calculate count_reg
			compare =  1'b0;
			pre_comp =  1'b0;

			av_update = 1'b0;

			find_write = 1'b0;
			write_pt_up = 1'b0;

			comp_write_pt_up = 1'b0;

			write_pointer_loading = 1'b0;
			
			comp_wr_done = 1'b0;
			comp_del_done = 1'b0;
			
			comp_del_start = 1'b0;
			comp_wr_start = 1'b0;
			
			data_buf_take = 1'b0;
			
			next_state = IDLE;
		end

		IDLE: 
		begin
			
			//if(rq_av)
			//begin
			//	wr_pt_up =  1'b1;
			//	next_state= WRITE_PRECHARGE;
			//end
			if(!full_add_cnt && comp_done)
			begin
				find_write = 1'b1;
				next_state = FIND_WRITE_LOC;
			end
			//else if(ear_global_time && comp_done) //begining
			//begin
			//	next_state = IDLE;
			//	pre_comp =  1'b1;
			//end
			else if(ear_global_time ) // this means earliest_time is reliable value.
			begin
				next_state = POP;
				next_pop =  1'b1;
				
				pre_comp = 1'b1;
			end
			else if(!comp_done) //now comparing
				next_state = COMPARE;
			else if(rq_av)
			begin
				data_buf_take = 1'b1;
				
				wr_pt_up = 1'b1;
				next_state = WRITE_PRECHARGE;
			end
			else
				next_state = IDLE;

		end
		WRITE: 
		begin

			write =  1'b1; // to write
			
			comp_wr_start = 1'b1;

			ack_this_previous =  1'b1;
			next_state = WRITE1;
		end
		WRITE1: 
		begin
			if(delay_past || delay_same) // past
			begin
				same_pt =  1'b1;
				
				comp_wr_done = 1'b1;
				
				next_state = POP;
			end
			else //not past
				next_state = WRITE_AFTER;
		end
		WRITE_AFTER: 
		begin
			next_state = IDLE;
			
			comp_wr_done = 1'b1;

			if(comp) //delay_wr < minimum delay
			begin
				next_rd_pt_write = 1'b1;
				//comp_wr_done = 1;
				wr_state =  2'b01;
			end
			else
				wr_state =  2'b00;
		end
		////////////////////////////
		// at this state, read data from register file
		//
		COMPARE: 
		begin
		//	compare = 1'b1;

			//if(rq_av) //write operation has priority
			//begin
			//	wr_pt_up = 1'b1;
			//	next_state = WRITE_PRECHARGE;
			//end
			//else 
			//begin
			//	compare = 1'b1;
			//	read =  1'b1;
			//	next_state = COMPARE_NEXT_PRE;
			//end
			if(comp_done)
				next_state = IDLE;
			else
			begin
				compare = 1'b1;
				read =  1'b1;
				
				wr_state = 2'b00;
				
				next_state = COMPARE_NEXT_PRE;
			end
		end
		////////////////////////////
		// at this state, compare value
		//
		COMPARE_NEXT_PRE:
		begin
			if(enable_col)
				next_state = COMPARE_NEXT;
		end
		COMPARE_NEXT: 
		begin
			rd_pt_up = 1'b1;
			
			next_state = COMPARE_NEXT1;
		
		end
		COMPARE_NEXT0:
		begin
			next_state = COMPARE_NEXT01;
		end
		COMPARE_NEXT01:
		begin
			next_state = COMPARE_NEXT02;
		end
		COMPARE_NEXT02:
		begin
			next_state = COMPARE_NEXT1;
		end
		COMPARE_NEXT1: 
		begin
			
			if(data_av)
			begin
				comp_del_start = 1'b1;
			
				next_state = COMPARE_NEXT2;
			end
			//else if(comp_done)
			//	next_state = IDLE;
			else
			//	next_state = IDLE;
				next_state = COMPARE;
		end
		////////////////////////////	
		// at this state, 
		//
		COMPARE_NEXT2: 
		begin
		
			comp_del_done = 1;
			
			//if(comp_done) 
			//begin
			//	wr_state = 2'b00;
			//	next_state = IDLE;
			//end
			//else if(same_time)
			if(same_time)
			begin
				wr_state = 2'b00;
				next_state = POP1;
			end
			else if(comp_del) //delay_com < minimum_delay
			begin
				next_rd_pt_read = 1'b1;
				wr_state =  2'b10;
				
				
				
				next_state = COMPARE;
			end
			else //to continue compare
			begin
				wr_state =  2'b00;
				next_state = COMPARE;
			end
		end
		POP:
		begin
			read =  1'b1;
			next_state = POP_READ_LATENCY1;
		end
		POP_READ_LATENCY1: 
		begin
			next_state = POP_READ_LATENCY2;
		end
		POP_READ_LATENCY2:
		begin
			next_state = POP1;
		end
		POP1: 
		begin
			req_this_next = 1'b1;
			if(ack_next_this)
			begin
				av_update = 1'b1; //data_buf = {0, data_out}
				next_state = WRITE_POINTER_LOADING;
			end
		end
		POP_WAIT: 
		begin
			write = 1'b1;
	
			//if(ack_next_this)
				next_state = IDLE;
		end
		FIND_WRITE_LOC:
		begin
			read = 1'b1;

			next_state = FIND_WRITE_LOC1;
		end
		FIND_WRITE_LOC1:
		begin
			if(enable_col)
				next_state = FIND_WRITE_LOC2;
		end
		FIND_WRITE_LOC2: 
		begin
			comp_write_pt_up = 1'b1; //to add 1 to write address
			if(!data_av) //if available place
			begin
				write_pt_up = 1'b1;
				next_state = FIND_WRITE_LOC3;
			end
			else
				next_state = IDLE;
		end
		FIND_WRITE_LOC3:
		begin
			next_state = IDLE;
		end
		WRITE_PRECHARGE:
		begin
			write_pointer_loading = 1'b1;
			next_state = WRITE;
		end
		WRITE_POINTER_LOADING:
		begin
			write_pointer_loading = 1'b1;
			next_state = POP_WAIT;
		end

				
	endcase
end


always @(posedge clk or posedge rst)
	if(rst)
		data_out <=  0;
	else if (enable_col)
		data_out <=  data_wire[31:0];
	else if(ack_next_this)
		data_out <=  0;
		
/////////////////////////
/// memory define
///
MEM_33x512 DUT(
  .clka(clk), // input clka
  .rsta(rst), // input rsta
  .ena(enable_read || enable_write), // input ena
  .wea(~enable_read || enable_write), // input [0 : 0] wea
  .addra(data_address), // input [8 : 0] addra
  .dina(data_buf), // input [32 : 0] dina
  .douta(data_wire) // output [32 : 0] douta
);


endmodule
