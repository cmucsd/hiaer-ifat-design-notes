`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:34:06 05/19/2011 
// Design Name: 
// Module Name:    USB_fifo_control_R0_1
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//     This block is designed for USB communication using FT245R chips provided by 
//     FTDI chip.
// Dependencies:
//     Datasheet of FT245r is available on http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT245R.pdf
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:
//     2011/7/14
//     First revision should not be very optimized yet in terms of timing. It may need to modified based on real implementation result.
//     However, it can be good to be a place holder for entire system aspect. 
//
//////////////////////////////////////////////////////////////////////////////////
module USB_fifo_control(
// general input
    input   clk, 
            rst,
// input & output connected to USB FIFO
    inout   [7:0] DATA,
    input   RXF_B,
    input   TXE_B,
    output  RD_B,
    output  WR,
    output  RD_ack, WR_ack,
// input & output connected to FPGA side     
    output  [7:0] fpga_in,      // input from fifo to fpga
    input   [7:0] fpga_out,     // output from fpga to fifo
	input	RD_req,				// read request to moderate rate of fifo reads to FPGA
    input   WR_req
    );
    ////////////////////////////
    //  pin description
    //
    //  RD_B : Enables the current FIFO data byte from D0...D7 when low. 
    //         Fetched the next FIFO data byte(if available) from the 
    //         receive FIFO buffer when RD_B goes from high to low.
    //  RXF_B : When high, do not read data from the FIFO. When low, 
    //          there is data available in the FIFO which can be read by strobing RD_B low, 
    //          then high again. During reset this signal pin is tri-state.
    //  TXE_B : When high, do not write data into the FIFO. 
    //          When low, data can be written into the FIFO by strobing WR high, then low. 
    //          During reset this signal pin is tri-state.
    //  WR : Writes the data from byte from D0...D7 pins into the transmit FIFO buffer when WR goes from high to low.

    reg     [7:0] DATA_in;  				// current data from USB port

	assign	DATA = WR? fpga_out:8'bz;       // on WR active high, route fpga_out to fifo data bus, otherwise turn off
    assign	fpga_in = DATA_in;

    assign	RD_B = ~(state == READ);
    assign	RD_ack = (state == READ);
    assign	WR = (state == WRITE);
    assign	WR_ack = (state == WRITE);
    
    parameter
        IDLE =  2'b00,
        READ =  2'b01,
        WRITE = 2'b10;

    reg [1:0] state;

// define clocked state transitions
    always @(posedge clk or posedge rst)
    begin
        if (rst)
            state <= IDLE;
        else
        begin
            case (state)
                IDLE:
                    if (RXF_B == 0 && RD_req == 1'b1)		// data available in fifo to be read from fifo into fpga
                        state <= READ;
                    else if (TXE_B == 0 && WR_req == 1'b1) 	// data available to be written into fifo from fpga
                        state <= WRITE;
                    else
                        state <= IDLE;
                READ:
					begin
						DATA_in <= DATA;
						state <= IDLE;
					end
                WRITE:
					state <= IDLE;
                default:
                    state <= IDLE;
            endcase
        end
    end    
endmodule
