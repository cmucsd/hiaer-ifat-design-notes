`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN Lab
// Engineer: Christoph Maier
// 
// Create Date:    11:41:43 10/10/2011 
// Design Name: 
// Module Name:    USB_top 
// Project Name:   HiAER-IFAT test board
// Target Devices: XC6SLX45T-3FGG484
// Tool versions: 
// Description: Simple USB port echo program
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module USB_top(
    input gclk_p, gclk_n, 		// global 200 MHz clock [H12, G11]
	input sysclk_p, sysclk_n, 	// local 200 MHz clock [AA12, AB12]
	input smaclk_p, smaclk_n,	// clock input from SMA connector [Y11, AB11]
    input RXF_B, TXE_B,         // FT245R indicators for Rx/Tx ready
    inout[7:0] DATA,            // FT245R data
    output RD_B, WR,            // FT245R control signals
    output[5:0] light			// indicator LEDs [E5, A2, B2, D5, D4, D3]
    );
	wire gclk, sysclk, smaclk;  // clock signals
    wire stateclk;              // 20 MHz UART state clock

    reg[7:0] datum;

    reg[2:0] state;
    parameter
        IDLE    = 3'o0,
        READ    = 3'o1,
        PROCESS = 3'o3,
        HOLD    = 3'o7,
        WRITE   = 3'o2;

	IBUFGDS #(.DIFF_TERM("FALSE"), .IOSTANDARD("DEFAULT")) 
	    gbuf (.O(gclk), .I(gclk_p), .IB(gclk_n)),
        sysbuf (.O(sysclk), .I(sysclk_p), .IB(sysclk_n)),
        smabuf (.O(smaclk), .I(smaclk_p), .IB(smaclk_n));
    div50M gdiv (.clock(gclk), .q(light[2])),
        sysdiv (.clock(sysclk), .q(light[1])),
        smadiv (.clock(smaclk), .q(light[0]));
    div10 prescaler (.clock(sysclk), .q(stateclk));

    assign DATA = WR ? datum : 8'bz;
//    assign light[2:0] = ~state[2:0];
    assign light[5] = TXE_B;
    assign light[4] = RXF_B;
    assign light[3] = (HOLD != state);
//    assign light[5:0] = datum[5:0];
    assign RD_B = (READ == state)? 1'b0 : 1'b1;
    assign WR = (WRITE == state)? 1'b1 : 1'b0;


    always @(posedge stateclk)
    begin
        case (state)
            IDLE:
            begin
                state <= RXF_B? IDLE : READ;
            end
            READ:
            begin
                state <= PROCESS;
            end
            PROCESS:
            begin
                state <= TXE_B ? HOLD : WRITE;
                datum <= DATA;
            end
            HOLD:
            begin
                state <= TXE_B ? HOLD : WRITE;
            end
            WRITE:
            begin
                state <= RXF_B ? IDLE : READ;
            end
            default:
            begin
                state <= IDLE;
            end
        endcase
    end
endmodule

// divide clock by 10
module div10(
	input clock,
	output q
	);

	reg[2:0] count;
	reg outbit;

	assign q=outbit;
	always @(posedge clock)
	begin
		if (count == 5)
		begin
			count <= 0;
			outbit <= ~outbit;
		end
		else
			count <= count+3'o1;
	end
endmodule

module div50M(
	input clock,
	output q
	);

	reg[25:0] count;
	reg outbit;

	assign q=outbit;
	always @(posedge clock)
	begin
		if (count == 0)
		begin
			count <= 49_999_999;
			outbit <= ~outbit;
		end
		else
			count <= count - 26'b1;
	end
endmodule