`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
//
// Create Date:   20:11:38 08/15/2012
// Design Name:   shift_sequencer
// Module Name:   C:/Xilinx/projects/IFAT_out_fifo_test/test_shift_sequencer.v
// Project Name:  dac
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: shift_sequencer
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_shift_sequencer;

	// Inputs
	reg clk;
	reg send;

	// Outputs
	wire address_strobe;
	wire shift;
	wire sclk;
	wire busy;

	// Instantiate the Unit Under Test (UUT)
	shift_sequencer uut (
		.clk(clk), 
		.send(send), 
		.shift(shift), 
		.sclk(sclk), 
		.busy(busy)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		send = 0;
        #21 send= 1;
        #5 send= 0;
	end

    always
        #2.5 clk= ~clk;
      
endmodule

