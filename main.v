`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:30:41 10/25/2011 
// Design Name: 
// Module Name:    DAC_control_v0 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main(
// general input
    input   sysclk_p, sysclk_n,     // local 200MHz clock [AA12, AB12]
	input	rst,

// USB signals
    inout   [7:0] DATA_USB,			// usb bidirectional data bus
    input   RXF_B, TXE_B,           // usb indicators for tx/rx ready
	output	RD_B, WR,				// usb control signals
 
// DAC signals
	output	CS_B_LD,				// dac control signals
    output	SCK,
    output	SDI,

// IFAT signals	
	output	EXT_HIAER_B,			// IFAT setup signals
	output	EXT_IFAT_B,

	output	IFAT_clk,				// IFAT arbitration clock
	output	RST_B,					// IFAT_AER reset
	output	RESETLFSR_B,			// IFAT arbitration LFSR reset
	output	RESETIFAT_B,			// IFAT arbitration reset

	output	[22:0] EXT_IN_TOP,					// external inputs from FPGA into IFAT
	output	[22:0] EXT_IN_BOTTOM,
	output	EXT_INREQ_TOP, EXT_INREQ_BOTTOM,
	input	EXT_INACK_TOP, EXT_INACK_BOTTOM,
	input	[22:0] EXT_OUT_TOP,					// external outputs from IFAT into FPGA
	input	[22:0] EXT_OUT_BOTTOM,
	input	EXT_OUTREQ_TOP, EXT_OUTREQ_BOTTOM,
	output	EXT_OUTACK_TOP, EXT_OUTACK_BOTTOM,

	output 	[5:0] LED_OFF
    );
// derive single-ended clock from differential clock signal
	wire 	sysclk;
	IBUFGDS #(.DIFF_TERM("FALSE"), .IOSTANDARD("DEFAULT")) sysbuf (.O(sysclk), .I(sysclk_p), .IB(sysclk_n));    
	wire 	clk;
	wire	clk2;

// USB controller module
    wire    RXF_B, TXE_B;
    wire    USB_RD_ack, USB_WR_ack;
    wire    RD_B, WR;
	wire	USB_IE;
    wire	USB_OE;
    wire	rst_USB;
	
    wire    [7:0] fpga_in;
    wire    [7:0] fpga_out;
    wire    [7:0] DATA_USB;			// direct connection between USB controller on fpga and USB fifo

// DAC signals
    wire    CS_B_LD;
    wire    SCK;
    wire    SDI;
	wire	DAC_req;
	wire	DAC_ack;
	wire	rst_DAC;

// IFAT signals
	reg		EXT_HIAER_B;			// disable HIAER (active high)
	reg		EXT_IFAT_B;				// disable IFAT	(active high)
     
//	reg		IFAT_clk;
	wire	IFAT_clk;
	reg		RST_B;
	reg		RESETLFSR_B;
	reg		RESETIFAT_B;

	reg		[22:0] EXT_IN_TOP;
	wire	EXT_INACK_TOP, EXT_OUTREQ_TOP;
	reg		EXT_INREQ_TOP, EXT_OUTACK_TOP;
	reg		[22:0] EXT_IN_BOTTOM;
	wire	EXT_INACK_BOTTOM, EXT_OUTREQ_BOTTOM;
	reg		EXT_INREQ_BOTTOM, EXT_OUTACK_BOTTOM;

// local variables 
	reg		[3:0] DAC_COMMAND;
    reg     [3:0] DAC_ADDR;
	reg		[15:0] DAC_DATA;
	reg		[2:0] DAC_CHIP;

	reg		[159:0] DAC_DATA_BATCH;					// DAC batch load write string
	reg 	[15:0] DAC_values_array [4:0][7:0];		// DAC data array -- chip num, dac num, 16 bit dac value
	reg		[5:0] daccount;							// index to iterate number of dacs
	reg		[5:0] dacmaxnum;						// maximum number of dacs to write to
	reg		[2:0] dacchipnum;						// DAC chip index

	reg		[2:0] dacdataloadtype;

	wire	[5:0] LED_OFF;

    reg     [5:0] state;
	reg		[7:0] USB_input_reg;
	
    parameter
        RESET       				= 6'h00,
        WAIT        				= 6'h01,
		READ_DAC_ADDR_WAIT			= 6'h02,
		READ_DAC_ADDR 				= 6'h03,
		READ_DAC_DATA_UB_WAIT		= 6'h04,
		READ_DAC_DATA_UB 			= 6'h05,
		READ_DAC_DATA_LB_WAIT 		= 6'h06,
		READ_DAC_DATA_LB 			= 6'h07,
		PROCESS_DAC					= 6'h08,
        WRITE_B     				= 6'h09,
        WRITE       				= 6'h0a,
        LATCH       				= 6'h0b,
        LATCH_B     				= 6'h0c,
		READ_USB_WAIT				= 6'h0d,
		READ_USB					= 6'h0e,
		PROCESS_USB					= 6'h0f,
		ACK_USB						= 6'h10,
		DAC_DATA_LOAD_INIT 			= 6'h11,
		DAC_DATA_LOAD_WAIT			= 6'h12,
		LOAD_DAC_DATA_WAIT			= 6'h13,
		UPDATE_DAC_DATA_BATCH		= 6'h14,
		LOAD_DAC_ADDR_WAIT			= 6'h15,
		READ_DAC_DATA_ADDR_WAIT 	= 6'h16,
		READ_DAC_DATA_ADDR			= 6'h17,
		STORE_DAC_DATA				= 6'h18,
		LOAD_DAC_DATA				= 6'h1a,
		READ_DAC_DATA_CHIPNUM_WAIT	= 6'h1b,
		READ_DAC_DATA_CHIPNUM		= 6'h1c,
		READ_DAC_DATA_BATCH_PREP	= 6'h1d,		
		TOGGLE_IFAT_MODE			= 6'h1e,
		ENABLE_IFAT					= 6'h1f,
		TEST_IFAT_START				= 6'h20,
		TEST_IFAT_END				= 6'h21,
		TOGGLE_IFAT_TOP_ACK			= 6'h22;
		
	parameter
		DAC_COMMAND_ON	= 4'b0011,			// write to and update (power up) dac n
		DAC_COMMAND_OFF	= 4'b1111;			// no operation
		
	parameter								// DAC load operation types
		DAC_LOAD_N		= 3'b000,
		DAC_LOAD_BATCH 	= 3'b001;
		
// define USB output combinatorial signals
	assign	rst_USB = (state == RESET);
	assign 	USB_IE = (state == READ_USB_WAIT || state == READ_DAC_ADDR_WAIT || 
						state == READ_DAC_DATA_ADDR_WAIT ||
						state == READ_DAC_DATA_CHIPNUM_WAIT || state == LOAD_DAC_DATA_WAIT ||
						state == READ_DAC_DATA_UB_WAIT || state == READ_DAC_DATA_LB_WAIT);
	assign	USB_OE = 1'b0;

// define DAC output combinatorial signals
	assign	rst_DAC = (state == RESET);
	assign	DAC_req = (state == DAC_DATA_LOAD_INIT);

// define IFAT output combinatorial signals
	assign	IFAT_clk = clk2;

// LED assignments for debugging and development
//	assign	LED_ON_0 = (state == WAIT);
	assign	LED_OFF[0] = SCK;
//	assign	LED_ON_2 = (RXF_B);
//	assign	LED_ON_3 = (state == WAIT);
//	assign	LED_ON_4 = (state == READ_USB_WAIT || state == READ_USB);
	assign	LED_OFF[1] = EXT_IN_TOP[18];
	assign	LED_OFF[2] = EXT_OUT_TOP[18];
	assign	LED_OFF[3] = ~(EXT_OUT_TOP[22] || EXT_OUT_TOP[21] || EXT_OUT_TOP[20] || 
						EXT_OUT_TOP[19] || EXT_OUT_TOP[17] || EXT_OUT_TOP[16] ||
						EXT_OUT_TOP[15] || EXT_OUT_TOP[14] || EXT_OUT_TOP[12] || 
						EXT_OUT_TOP[11] || EXT_OUT_TOP[10] || EXT_OUT_TOP[9] || 
						EXT_OUT_TOP[8] || EXT_OUT_TOP[7] || EXT_OUT_TOP[6] || 
						EXT_OUT_TOP[5] || EXT_OUT_TOP[4] || EXT_OUT_TOP[3] || 
						EXT_OUT_TOP[2] || EXT_OUT_TOP[1] || EXT_OUT_TOP[0]);
	assign	LED_OFF[4] = EXT_INACK_TOP;
//	assign 	LED_ON_4 = IFAT_clk;
//	assign	LED_ON_3 = (EXT_IN_TOP13);
	assign	LED_OFF[5] = EXT_OUTREQ_TOP;


// define clocked state transitions
    always @(posedge clk or posedge rst)
	begin
		if (rst)
			state <= RESET;
        else
        begin
            case (state)
            RESET : 
                begin
					EXT_HIAER_B <= 1'b1;					// initialize HIAER operation to disabled, will remain disabled
					EXT_IFAT_B <= 1'b1;						// initialize IFAT operation to disabled

//					IFAT_clk <= 1'b0;
					
					RST_B <= 1'b1;							// reset all components	
					RESETLFSR_B <= 1'b1;
					RESETIFAT_B <= 1'b1;

					EXT_IN_TOP <= 23'h000000;
					EXT_INREQ_TOP <= 1'b0;
					EXT_OUTACK_TOP <= 1'b0;

					EXT_IN_BOTTOM <= 23'h000000;
					EXT_INREQ_BOTTOM <= 1'b0;
					EXT_OUTACK_BOTTOM <= 1'b0;

// DACs are addressed with 0-->U15, 1-->U14, ... 4-->U12				
					DAC_values_array[4][0] = 16'hcccd;		// Vbias = 1.2V
					DAC_values_array[4][1] = 16'h5555;		// VpupArbit = 0.5V
					DAC_values_array[4][2] = 16'h5555;		// VbnArbit = 0.5V
					DAC_values_array[4][3] = 16'h5555;		// VbpArbit = 0.5V
					DAC_values_array[4][4] = 16'h5555;		// VbpLogCell = 0.5V
					DAC_values_array[4][5] = 16'h2222;		// VpupReq = 0.2V
					DAC_values_array[4][6] = 16'hffff;		// Vspike = 1.5V
					DAC_values_array[4][7] = 16'h5555;		// Vpdn = 0.5V
					
					DAC_values_array[3][0] = 16'h5555;		// Vtau_0_top = 0.5V
					DAC_values_array[3][1] = 16'h5555;		// Vtau_1_top = 0.5V
					DAC_values_array[3][2] = 16'h5555;		// Vtau_2_top = 0.5V
					DAC_values_array[3][3] = 16'h5555;		// Vtau_3_top = 0.5V
					DAC_values_array[3][4] = 16'hffff;		// Erev_0_top = 1.5V
					DAC_values_array[3][5] = 16'hffff;		// Erev_1_top = 1.5V
					DAC_values_array[3][6] = 16'hffff;		// Erev_2_top = 1.5V
					DAC_values_array[3][7] = 16'hffff;		// Erev_3_top = 1.5V

					DAC_values_array[2][0] = 16'h5555;		// Vtau_0_bottom = 0.5V
					DAC_values_array[2][1] = 16'h5555;		// Vtau_1_bottom = 0.5V
					DAC_values_array[2][2] = 16'h5555;		// Vtau_2_bottom = 0.5V
					DAC_values_array[2][3] = 16'h5555;		// Vtau_3_bottom = 0.5V
					DAC_values_array[2][4] = 16'hffff;		// Erev_0_bottom = 1.5V
					DAC_values_array[2][5] = 16'hffff;		// Erev_1_bottom = 1.5V
					DAC_values_array[2][6] = 16'hffff;		// Erev_2_bottom = 1.5V
					DAC_values_array[2][7] = 16'hffff;		// Erev_3_bottom = 1.5V

					DAC_values_array[1][0] = 16'hffff;		// VinMax_top = 1.5V
					DAC_values_array[1][1] = 16'h0000;		// Vreset_top = 0V
					DAC_values_array[1][2] = 16'h0000;		// Vthresh_top = 0V
					DAC_values_array[1][3] = 16'hffff;		// Gcomp_top = 1.5V
					DAC_values_array[1][4] = 16'h5555;		// Gleak_top = 0.5V
					DAC_values_array[1][5] = 16'hffff;		// Eleak_top = 1.5V
					DAC_values_array[1][6] = 16'hffff;		// xxx = 1.5V
					DAC_values_array[1][7] = 16'hffff;		// xxx = 1.5V

					DAC_values_array[0][0] = 16'hffff;		// VinMax_bottom = 1.5V
					DAC_values_array[0][1] = 16'h0000;		// Vreset_bottom = 0V
					DAC_values_array[0][2] = 16'h0000;		// Vthresh_bottom = 0V
					DAC_values_array[0][3] = 16'hffff;		// Gcomp_bottom = 1.5V
					DAC_values_array[0][4] = 16'h5555;		// Gleak_bottom = 0.5V
					DAC_values_array[0][5] = 16'hffff;		// Eleak_bottom = 1.5V
					DAC_values_array[0][6] = 16'hffff;		// xxx = 1.5V
					DAC_values_array[0][7] = 16'hffff;		// xxx = 1.5V

                    state <= WAIT;
                end
            WAIT :
                begin
					if (RXF_B == 0)							// wait for DATA available on USB FIFO
						state <= READ_USB_WAIT;
					else
						state <= WAIT;
                end

// start main loop: wait for USB input then parse USB command				
			READ_USB_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_USB_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_USB;					
				end
			READ_USB :
				begin
					USB_input_reg <= fpga_in;				// load USB data into FPGA
					state <= PROCESS_USB;
				end
			PROCESS_USB :									// parse USB commands
				begin
					if (USB_input_reg == 8'hd0)				// COMMAND -- load n DAC values from USB
						begin
							daccount <= 6'b000000;
							dacdataloadtype <= DAC_LOAD_N;
							state <= LOAD_DAC_DATA_WAIT;
						end
					else if (USB_input_reg == 8'hd1)		// COMMAND -- load 5 DAC values from USB
						begin
							daccount <= 6'b000000;
							dacmaxnum <= 6'b000100;
							DAC_CHIP <= 3'b000;
							dacdataloadtype <= DAC_LOAD_BATCH;
							state <= READ_DAC_DATA_BATCH_PREP;
						end
					else if (USB_input_reg == 8'he0)		// COMMAND -- load 5 DAC addresses, then write DAC values
						begin
							daccount <= 6'b000000;
							dacmaxnum <= 6'b000100;
							DAC_CHIP <= 3'b000;
							DAC_DATA_BATCH <= 160'h0000000000000000000000000000000000000000;
							state <= READ_DAC_ADDR_WAIT;
						end
					else if (USB_input_reg == 8'h10)		// COMMAND -- toggle FPGA --> IFAT interface
						state <= TOGGLE_IFAT_MODE;
					else if (USB_input_reg == 8'h11)		// COMMAND -- toggle IFAT TOP ACK
						state <= TOGGLE_IFAT_TOP_ACK;
					else if (USB_input_reg == 8'h20)		// COMMAND -- enable IFAT internal operation mode
						state <= ENABLE_IFAT;
					else if (USB_input_reg == 8'h30)		// COMMAND -- start IFAT activity for testing
						state <= TEST_IFAT_START;
					else if (USB_input_reg == 8'h31)		// COMMAND -- end IFAT activity for testing
						state <= TEST_IFAT_END;
					else
						state <= WAIT;
				end

// load a n DAC analog values from the USB to be loaded into internal memory
            LOAD_DAC_DATA_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= LOAD_DAC_DATA_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= LOAD_DAC_DATA;
				end
            LOAD_DAC_DATA :
				begin
					dacmaxnum <= fpga_in[5:0];				// store maximum number of dac values
					state <= READ_DAC_DATA_CHIPNUM_WAIT;	// continue loading DAC values
				end
			READ_DAC_DATA_CHIPNUM_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_DAC_DATA_CHIPNUM_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_DAC_DATA_CHIPNUM;
				end
			READ_DAC_DATA_CHIPNUM :
				begin
					DAC_CHIP <= fpga_in[2:0] + 3'b001;		// load dac chip number, add one for format of value load
					state <= READ_DAC_DATA_ADDR_WAIT;
				end

// load a full string of 5 DAC addresses from the USB to be read from internal memory and batch loaded onto the DACs
			READ_DAC_DATA_BATCH_PREP :
				begin
					DAC_CHIP <= DAC_CHIP + 3'b001;			// load dac chip number, add one for format of value load
					state <= READ_DAC_DATA_ADDR_WAIT;
				end

// general routine to load DAC analog data values from USB into internal FPGA memory
            READ_DAC_DATA_ADDR_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_DAC_DATA_ADDR_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_DAC_DATA_ADDR;
				end
            READ_DAC_DATA_ADDR :
				begin
					DAC_ADDR <= fpga_in[3:0];				// load dac addr
					state <= READ_DAC_DATA_UB_WAIT;
				end
            READ_DAC_DATA_UB_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_DAC_DATA_UB_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_DAC_DATA_UB;
				end
            READ_DAC_DATA_UB :
				begin
					DAC_DATA[15:8] <= fpga_in;				// load dac data upper byte
					state <= READ_DAC_DATA_LB_WAIT;
				end
            READ_DAC_DATA_LB_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_DAC_DATA_LB_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_DAC_DATA_LB;
				end
            READ_DAC_DATA_LB :
				begin
					DAC_DATA[7:0] <= fpga_in;				// load dac data lower byte
					daccount <= daccount + 6'd1;			// update dac number
					state <= STORE_DAC_DATA;
				end
			STORE_DAC_DATA :
				begin
					DAC_values_array[DAC_CHIP-1][DAC_ADDR] <= DAC_DATA;
					if (daccount > dacmaxnum)
						state <= WAIT;
					else
						if (dacdataloadtype == DAC_LOAD_N)
							state <= READ_DAC_DATA_CHIPNUM_WAIT;
						else
							state <= READ_DAC_DATA_BATCH_PREP;
				end

// load a full string of DAC addresses from the USB to be read from internal memory and batch loaded onto the DACs
            READ_DAC_ADDR_WAIT :
				begin
					if (USB_RD_ack == 1'b0)
						state <= READ_DAC_ADDR_WAIT;
					else if (USB_RD_ack == 1'b1)
						state <= READ_DAC_ADDR;
				end
            READ_DAC_ADDR :
				begin
					DAC_ADDR <= fpga_in[3:0];				// load dac addr
					DAC_CHIP <= DAC_CHIP + 3'b001;			// increment dac chip index
					state <= LOAD_DAC_ADDR_WAIT;
				end
			LOAD_DAC_ADDR_WAIT :
				begin
					daccount <= daccount + 6'd1;			// update dac number
					DAC_DATA <= DAC_values_array[DAC_CHIP-1][DAC_ADDR];
					if (DAC_ADDR > 4'b0111 && DAC_ADDR < 4'b1111)
						DAC_COMMAND <= DAC_COMMAND_OFF;
					else
						DAC_COMMAND <= DAC_COMMAND_ON;
					state <= UPDATE_DAC_DATA_BATCH;
				end
			UPDATE_DAC_DATA_BATCH :
				begin
					DAC_DATA_BATCH <= {DAC_DATA_BATCH[127:0], 8'h00, DAC_COMMAND, DAC_ADDR, DAC_DATA};
					if (daccount > dacmaxnum)
						state <= DAC_DATA_LOAD_INIT;
					else
						state <= READ_DAC_ADDR_WAIT;
				end

// with DAC data loaded, allow DAC controller to load values
			DAC_DATA_LOAD_INIT :
				begin
					state <= DAC_DATA_LOAD_WAIT;
				end
			DAC_DATA_LOAD_WAIT :
				begin
					if (DAC_ack == 1)
						state <= WAIT;
					else
						state <= DAC_DATA_LOAD_WAIT;
				end

// toggle FPGA --> IFAT interface
			TOGGLE_IFAT_MODE :
				begin
					EXT_IFAT_B <= ~EXT_IFAT_B;
					state <= WAIT;
				end

// toggle IFAT TOP ACK
			TOGGLE_IFAT_TOP_ACK :
				begin
					EXT_OUTACK_TOP <= ~EXT_OUTACK_TOP;
					state <= WAIT;
				end				
				
// enable internal IFAT operation (IFAT_AER, LFSR, AER)
			ENABLE_IFAT :
				begin
					RST_B <= 1'b0;							// enablet all components	
					RESETLFSR_B <= 1'b0;
					RESETIFAT_B <= 1'b0;
					state <= WAIT;
				end

			TEST_IFAT_START :
				begin
					EXT_IN_TOP[5:0] <= 6'h3F;				// set synapse strength to full on
					EXT_IN_TOP[18] <= 1'b1;
					EXT_IN_TOP[13] <= 1'b1;
					state <= WAIT;
				end
				
			TEST_IFAT_END :
				begin
					EXT_IN_TOP[5:0] <= 6'h00;				// turn off synapse strength
					EXT_IN_TOP[18] <= 1'b0;
					EXT_IN_TOP[13] <= 1'b0;
					state <= WAIT;
				end

            default :
				begin
					state <= WAIT;
				end
			endcase
		end
	end

	clk_divider_dac 	clk_div_dac(
		.sysclk(sysclk), 
		.clk_slow(clk));
	clk_divider_ifat 	clk_div_ifat(
		.sysclk(sysclk), 
		.clk_slow(clk2));		
	DAC_batch_load		dac_batch_loader(
		.clk(clk),
		.rst(rst_DAC),
		.DAC_req(DAC_req),
		.DAC_DATA_BATCH(DAC_DATA_BATCH),
		.CS_B_LD(CS_B_LD),
		.SCK(SCK),
		.SDI(SDI),
		.DAC_done(DAC_ack));
    USB_fifo_control 	usb_dac_interface(
        .clk(clk), 
        .rst(rst_USB), 
        .DATA(DATA_USB), 
        .RXF_B(RXF_B), 
        .TXE_B(TXE_B), 
        .RD_B(RD_B), 
        .WR(WR), 
        .RD_ack(USB_RD_ack), 
        .WR_ack(USB_WR_ack), 
        .fpga_in(fpga_in), 
        .fpga_out(fpga_out), 
		.RD_req(USB_IE),
        .WR_req(USB_OE));
endmodule

module clk_divider_dac(
	input	sysclk,
	output	clk_slow
	);
	
	reg	[24:0] count;
	reg	outbit;
	
	assign clk_slow = outbit;
	always @(posedge sysclk)
	begin
//		if (count == 10)			// subdivide 200MHz sysclk into 20MHz clk
		if (count == 2000000)			// subdivide 200MHz sysclk into 20MHz clk
		begin
			count <= 0;
			outbit <= ~outbit;
		end
		else
			count <= count+1;
	end
endmodule

module clk_divider_ifat(
	input	sysclk,
	output	clk_slow
	);
	
	reg	[24:0] count;
	reg	outbit;
	
	assign clk_slow = outbit;
	always @(posedge sysclk)
	begin
//		if (count == 10)			// subdivide 200MHz sysclk into 20MHz clk
		if (count == 16000000)			// subdivide 200MHz sysclk into 20MHz clk
		begin
			count <= 0;
			outbit <= ~outbit;
		end
		else
			count <= count+1;
	end
endmodule
