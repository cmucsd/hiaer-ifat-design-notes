`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN    Lab
// Engineer: Christoph Maier
// 
// Create Date:     16:28:14 09/21/2011 
// Design Name:     HiAER-IFAT v.1.0 test program
// Module Name:     blinkdit 
// Project Name:    Neovision 2
// Target Devices:  xc6slx45t-3fgg484
// Tool versions:   ISE 13.2
// Description:     Divides the 3 differential clock inputs 
//                  to the L0 and L1 FPGAs and shows them on status LEDs
// Dependencies:    None
//
// Revision: 1.1
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module blinkdit(
    input gclk_p, gclk_n,        // global 200 MHz clock [H12, G11]
    input sysclk_p, sysclk_n,    // local 200 MHz clock [AA12, AB12]
    input smaclk_p, smaclk_n,    // clock input from SMA connector [Y11, AB11]
    output[5:0] light            // indicator LEDs [E5, A2, B2, D5, D4, D3]
    );
    wire gclk, sysclk, smaclk;

    IBUFGDS #(.DIFF_TERM("FALSE"), .IOSTANDARD("DEFAULT")) 
        gbuf (.O(gclk), .I(gclk_p), .IB(gclk_n)),
        sysbuf (.O(sysclk), .I(sysclk_p), .IB(sysclk_n)),
        smabuf (.O(smaclk), .I(smaclk_p), .IB(smaclk_n));
    blinker 
        gblink(.clock(gclk), .qh(light[5]), .ql(light[2])),
        sysblink(.clock(sysclk), .qh(light[4]), .ql(light[1])),
        smablink(.clock(smaclk), .qh(light[3]), .ql(light[0]));
endmodule

module blinker(
    input clock,
    output qh, ql
    );
    wire tc;
    divider div(.clock(clock), .q(tc));
    twobitcount ct(.clock(tc), .ch(qh), .cl(ql));
endmodule

module divider(
    input clock,
    output q
    );

    reg[27:0] count;
    reg outbit;

    assign q=outbit;
    always @(posedge clock)
    begin
        if (count == 99999999)
        begin
            count <= 0;
            outbit <= ~outbit;
        end
        else
            count <= count+1;
    end
endmodule

module twobitcount(
    input clock,
    output ch, cl
    );
    reg[1:0] c;

    assign ch=c[1], cl=c[0];

    always @(posedge clock)
    begin
        c <= c+2'b01;
    end
endmodule
