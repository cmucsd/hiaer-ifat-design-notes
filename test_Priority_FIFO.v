`timescale 1ns/1ps

module test_Priority_FIFO;


//general input
reg clk, rst;
reg [31:0] data_in;
reg req_previous_this;

//input
reg [9:0] global_time;
reg ack_next_this;

//output
//wire [31:0] data_out;
wire [21:0] data_out_alias;
wire req_this_next;
wire ack_this_previous;
wire buffer_full;
wire buffer_empty;

Priority_FIFO DUT(
clk, rst, data_in, req_previous_this, 
global_time, ack_next_this, 
data_out_alias, req_this_next, ack_this_previous, buffer_full, buffer_empty);

initial
begin
	clk = 1'b0;
	rst = 1'b0;
	req_previous_this = 1'b0;
	data_in = 32'b0;

	ack_next_this = 1'b0;

	#1 rst = 1'b1;
	#3 rst = 1'b0;
end
always@(posedge clk)
	if(ack_this_previous) // if priority queue sends acknowledge at the input
	begin
	if(req_previous_this) // and a request was initiated
	begin
		req_previous_this <= #0.4 1'b0; // negate the request
		data_in <= #0.4 32'h0; // and clear the data
	end
	end


initial
begin
	global_time = 780; //0
	#4000 global_time = 781;
	#4000 global_time = 782;
	#4000 global_time = 783;
	#4000 global_time = 784;
	#4000 global_time = 785;
	#4000 global_time = 786;
	#4000 global_time = 787;
	#4000 global_time = 788; 
	#4000 global_time = 789;
	#4000 global_time = 790;
	#4000 global_time = 791;
	#4000 global_time = 792;
end

task request;
	input [21:0] data;
	input [9:0] timestamp;
	begin
	req_previous_this = 1'b1;
	
	data_in = {data, timestamp};
	end
endtask

initial
begin
	#2000 request(540, 540);
	#800 request(900, 900);
	#800 request(760, 760);
	#800 request(784, 784);
	#2000 request(800, 800); //5
	#1000 request(790, 790);
	#1000 request(787, 787);
	#4800 request(787, 787);
	#200 request(777, 777);
	#1000 request(760, 760); //10
	#400 request(788, 788);
	#1000 request(799, 799);
	#600 request(810, 810);
	#800 request(801, 801);
	#1000 request(799, 799); //15
	#800 request(820, 820); //16
	#400 request(780, 780);
end

initial
begin
	#24800 request(791, 791);
	#2000 request(800, 800);
end
initial
begin
	#28000 request(787, 787);
end
always @(posedge clk)
	if(ack_next_this)
		ack_next_this = 1'b0;
	else if(req_this_next)
		ack_next_this = 1'b1;
	else
		ack_next_this = 1'b0;
always
	clk = #0.50 ~clk;

initial 
	#50000 $stop;

always@(posedge clk)
	if(ack_next_this)
		$display("output data is %d at time %d at global_time %d", data_out_alias, $time, global_time); 

always@(posedge clk)
	if(ack_this_previous)
		$display("input data is %d at time %d at global_time %d", data_in[31:10], $time, global_time); 

endmodule
