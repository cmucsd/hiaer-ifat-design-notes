//
//
//  Priority buffer
//
//  Jongkil Park
//
//  We use 1GHz clock.
//  
//  make more synthesizable
`timescale 1ns/1ps

module Priority_buffer(
//general input
clk, rst, 
//input
data_in, request, pop_request,

//output
data_out, output_request_d, ack

);

//general input
input clk, rst;

//input
input [21:0] data_in; //32bits data
input request; // input data is available.
input pop_request; // ack signal from next level module

//output
output [21:0] data_out;
output output_request_d; 
output ack; //output to previous level module


//output relate register
reg [21:0] data_out;
reg output_request_d; //when quese is not empty, it wants to send a output to next block.

//internal register
reg [4:0] count_fifo;
reg requesting;
reg enable_read, enable_write;
reg data_en;
reg [21:0] data_buf;
reg ack;

reg output_request;

reg [15:0] write_location, read_location;


wire [21:0] data_wire_bar;  //data out from register file.
wire [21:0] data_wire_bar1;

wire buffer_full, buffer_empty;
reg read_latency;


/////////////////
// buffer full
//
assign buffer_full = (count_fifo == 5'b01111)?1'b1:1'b0;
assign buffer_empty = (count_fifo == 5'b00000)?1'b1:1'b0;


////////////////////
// requesting
//
always @(posedge clk or posedge rst)
	if(rst)
	begin
		output_request <=  1'b0;
		requesting <=  1'b0;
	end
	else	if(!buffer_empty && !requesting)
	begin
		output_request <=  1'b1;
		requesting <=  1'b1;
	end
	else	if(pop_request)
	begin	
		output_request <=  1'b0;
		requesting <=  1'b0;
	end
	else	if(requesting)
	begin
		output_request <=  1'b0;
		requesting <=  1'b1;
	end

always @(posedge clk or posedge rst)
	if(rst)
		output_request_d <= 1'b0;
	else
		output_request_d <= output_request;


////////////////////
// accept request signal
//
always @ (posedge clk or posedge rst)
	if(rst)
		data_buf <=  22'h0;
	else if(request)
		data_buf <=  data_in;


////////////////////
//  fifo_count
//
always @(posedge clk or posedge rst)
	if(rst)
		count_fifo <=  0;
	else	if(pop_request && ack)
		count_fifo <=  count_fifo;
	else	if(pop_request) // read
		count_fifo <=  count_fifo - 1'b1;
	else	if(ack) // write
		count_fifo <=  count_fifo + 1'b1;


////////////////////
//  data read
//
always @(posedge clk or posedge rst ) // read
	if(rst)
		read_location <=  16'b0000_0000_0000_0001;
	else	if(pop_request)
		read_location <=  {read_location[14:0], read_location[15]};

always @(posedge clk or posedge rst)
	if(rst)
		enable_read <=  1'b0;
	else if(!buffer_empty && !requesting)
		enable_read <=  1'b1;
	else if(read_latency == 1'b1)
		enable_read <=  1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		read_latency <= 1'b0;
	else if(enable_read)
		read_latency <= 1'b1;
	else
		read_latency <= 1'b0;


////////////////////
//  data write
//
always @(posedge clk or posedge rst)//write
	if(rst)
		enable_write <=  1'b0;
	else	if(request) //write
		enable_write <=  1'b1;
	else //default
		enable_write <=  1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		write_location <=  16'b0000_0000_0000_0001;
	else if(ack)
		write_location <=  {write_location[14:0], write_location[15]};

//////////////////////
// ack signal
// internal purpose

always @(posedge clk or posedge rst)
	if(rst)
		ack <=  1'b0;
	else if(request&&!buffer_full)
		ack <=  1'b1;
	else
		ack <=  1'b0;

always @(posedge clk or posedge rst)
	if(rst)
		data_out <=  22'h0;
	else if(enable_read && read_latency)
		data_out <=  ~data_wire_bar;
	else
		data_out <=  data_out;

reg22x16 regfile(data_wire_bar, data_wire_bar1, data_buf, read_location, write_location, enable_write, enable_read);

endmodule

