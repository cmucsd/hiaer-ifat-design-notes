`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:11:42 03/15/2010 
// Design Name: 
// Module Name:    analog_arbit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module input_to_al(
  clk, rst,
  /*
  INSERT THE Pins for EXTERNAL CONTROL !
  */
    req_dl,
    ack_dl,
    fifo_in_dl,   
    if0_out_al,
    if0_en,  
    if1_out_al,
    if1_en,
    if2_out_al,
    if2_en,
    if3_out_al,
    if3_en
    );

  input rst, clk;

   input req_dl;
   output ack_dl;
    input [20:0] fifo_in_dl;    
    output [12:0] if0_out_al;    
    output [12:0] if1_out_al;
    output [12:0] if2_out_al;
    output [12:0] if3_out_al;
    output if0_en, if1_en, if2_en, if3_en;

parameter /*synopsys enum state*/
state0_0 = 2'b00, state1_0 = 2'b01, state2_0 = 2'b10, state3_0 = 2'b11,
state0_1 = 2'b00, state1_1 = 2'b01, state2_1 = 2'b10, state3_1 = 2'b11,
state0_2 = 2'b00, state1_2 = 2'b01, state2_2 = 2'b10, state3_2 = 2'b11,
state0_3 = 2'b00, state1_3 = 2'b01, state2_3 = 2'b10, state3_3 = 2'b11;

//synopsys state_vector sr0
reg [1:0] sr0, ns0;
//synopsys state_vector sr1
reg[1:0] sr1, ns1;
//synopsys state_vector sr2
reg [1:0] sr2, ns2;
//synopsys state_vector sr3
reg [1:0] sr3, ns3;
reg pop_request;
reg[12:0] if0_out_al, if1_out_al, if2_out_al, if3_out_al;
reg if0_req_al, if1_req_al,if2_req_al, if3_req_al;
reg if0_en, if1_en, if2_en, if3_en;
wire [20:0] data_out;
reg [20:0] data_reg;
reg fifo_ack_al;
reg [12:0] fifo_out_al;
reg fifo_req_al;


reg load_data_en,load_data, dec_cntr, en_data_out , enable_col_row;
//reg [20:0] data_in;
reg request;
reg ack_dl;
reg en_out0,en_out1,en_out2,en_out3;
reg dcnt0, dcnt1, dcnt2, dcnt3;
reg [5:0] counter0, counter1, counter2, counter3;
reg [18:0] din0, din1, din2, din3;
reg req0, req1, req2, req3;
reg pop_req0,pop_req1,pop_req2,pop_req3;
wire [21:0] dout0, dout1, dout2, dout3;

Priority_buffer P_buff0(.clk(clk), .rst(rst), .data_in({1'b0,1'b0,1'b0,din0}), .request(req0), .pop_request(pop_req0),.data_out(dout0), .output_request_d(op_req0), .ack(bfull0));
Priority_buffer P_buff1(.clk(clk), .rst(rst), .data_in({1'b0,1'b0,1'b0,din1}), .request(req1), .pop_request(pop_req1),.data_out(dout1), .output_request_d(op_req1), .ack(bfull1));
Priority_buffer P_buff2(.clk(clk), .rst(rst), .data_in({1'b0,1'b0,1'b0,din2}), .request(req2), .pop_request(pop_req2),.data_out(dout2), .output_request_d(op_req2), .ack(bfull2));
Priority_buffer P_buff3(.clk(clk), .rst(rst), .data_in({1'b0,1'b0,1'b0,din3}), .request(req3), .pop_request(pop_req3),.data_out(dout3), .output_request_d(op_req3), .ack(bfull3));


/* assign the different 4 FIFO based on what the data value is */

always @ (bfull0,bfull1,bfull2,bfull3, req_dl, fifo_in_dl)
if (req_dl == 1'b1) /* depending on what the data term says - connect to required fifo */
begin
	case (fifo_in_dl[20:19])
	0: begin
	req0 = req_dl;
	din0 = fifo_in_dl[18:0];
	ack_dl = bfull0;
	req1 = 1'b0;
	din1 = 19'b0;
	req2 = 1'b0;
	din2 = 19'b0;
	req3 = 1'b0;
	din3 = 19'b0;
	end
	
	1: begin
	req1 = req_dl;
	din1 = fifo_in_dl[18:0];
	ack_dl = bfull1;
	req0 = 1'b0;
	din0 = 19'b0;
	req2 = 1'b0;
	din2 = 19'b0;
	req3 = 1'b0;
	din3 = 19'b0;
	end
	
	2: begin
	req2 = req_dl;
	din2 = fifo_in_dl[18:0];
	ack_dl = bfull2;
	req1 = 1'b0;
	din1 = 19'b0;
	req0 = 1'b0;
	din0 = 19'b0;
	req3 = 1'b0;
	din3 = 19'b0;
	end
	
	3: begin
	req3 = req_dl;
	din3 = fifo_in_dl[18:0];
	ack_dl = bfull3;
	req1 = 1'b0;
	din1 = 19'b0;
	req2 = 1'b0;
	din2 = 19'b0;
	req0 = 1'b0;
	din0 = 19'b0;
	end
		
	endcase

end
else
begin /* set stuff to 0 */
	req3 = 1'b0;
	din3 = 19'b0;
	ack_dl = 1'b0;
	req1 = 1'b0;
	din1 = 19'b0;
	req2 = 1'b0;
	din2 = 19'b0;
	req0 = 1'b0;
	din0 = 19'b0;
end





always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		sr0 <= state0_0; 
		sr1 <= state0_1; 
		sr2 <= state0_2; 
		sr3 <= state0_3; 
	end  
 	else
	begin
		sr0<=ns0;
		sr1<=ns1;
		sr2<=ns2;
		sr3<=ns3;
	end
end



always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if0_out_al <= 13'b0; 
	end  
 	else
	begin	
		if (pop_req0==1'b1) 
			if0_out_al <= dout0[18:6];
		else
			if0_out_al <= if0_out_al;
	end
end

always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if1_out_al <= 13'b0; 
	end  
 	else
	begin	
		if (pop_req1==1'b1) 
			if1_out_al <= dout1[18:6];
		else
			if1_out_al <= if1_out_al;
	end
end



always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if2_out_al <= 13'b0; 
	end  
 	else
	begin	
		if (pop_req2==1'b1) 
			if2_out_al <= dout2[18:6];
		else
			if2_out_al <= if2_out_al;
	end
end

always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if3_out_al <= 13'b0; 
	end  
 	else
	begin	
		if (pop_req3==1'b1) 
			if3_out_al <= dout3[18:6];
		else
			if3_out_al <= if3_out_al;
	end
end


always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		counter0<=6'b0; 
		if0_en <=1'b0;
	end  
 	else
	begin
		if (pop_req0==1'b1) 
		begin
			counter0<=dout0[5:0];
			if0_en <=1'b0;
		end
		else
			if (dcnt0==1'b1)
			begin
				counter0<=counter0-1'b1;
				if0_en <=1'b1;
			end
			else
			begin
				counter0<=counter0;
				if0_en <=1'b0;
			end
	end
end


always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if1_en <=1'b0;
		counter1<=6'b0; 
	end  
 	else
	begin
		if (pop_req1==1'b1) 
		begin
			if1_en <=1'b0;
			counter1<=dout1[5:0];
		end
		else
			if (dcnt1==1'b1)
			begin
				counter1<=counter1-1'b1;
				if1_en <=1'b1;
			end
			else
			begin
				counter1<=counter1;
				if1_en <=1'b0;
			end
	end
end



always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		counter2<=6'b0; 
		if2_en <=1'b0;
	end  
 	else
	begin
		if (pop_req2==1'b1) 
		begin
			counter2<=dout2[5:0];
			if2_en <=1'b0;
		end
		else
			if (dcnt2==1'b1)
			begin
				counter2<=counter2-1'b1;
				if2_en <=1'b1;
			end
			else
			begin
				counter2<=counter2;
				if2_en <=1'b0;
			end
	end
end


always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		if3_en <=1'b0;
		counter3<=6'b0; 
	end  
 	else
	begin
		if (pop_req3==1'b1) 
		begin
			if3_en <=1'b0;
			counter3<=dout3[5:0];
		end
		else
			if (dcnt3==1'b1)
			begin
				counter3<=counter3-1'b1;
				if3_en <=1'b1;
			end
			else
			begin
				counter3<=counter3;
				if3_en <=1'b0;
			end
	end
end


always@(sr0, op_req0, counter0)
begin
case(sr0)
state0_0: begin	
		en_out0 <= 1'b0;
		pop_req0 <= 1'b0;
		ns0 <= state1_0;
		dcnt0<=1'b0;
end

state1_0: begin
	if (op_req0 == 1'b1) 
	begin
			en_out0 <= 1'b1;
			pop_req0 <= 1'b1;
			ns0 <= state2_0;
			dcnt0<=1'b0;
	end
	else
	begin
			en_out0 <= 1'b0;
			pop_req0 <= 1'b0;
			ns0 <= state1_0;
			dcnt0<=1'b0;

	end
end

state2_0: begin
	en_out0<=1'b0;
	if (counter0 > 6'b0) 
	begin
		ns0<= state2_0;
		dcnt0 <= 1'b1;
			pop_req0<=1'b0;
	end
	else
	begin
		dcnt0<=1'b0;
		ns0<= state1_0;
			pop_req0<=1'b0;
	end
end

default:
begin
	en_out0 <= 1'b0;
	pop_req0 <= 1'b0;
	ns0 <= state1_0;
	dcnt0<=1'b0;
end
endcase
end

always@(sr1,op_req1, counter1)
begin
case(sr1)
state0_1: begin	
		en_out1 <= 1'b0;
		pop_req1 <= 1'b0;
		ns1 <= state1_1;
		dcnt1<=1'b0;
end

state1_1: begin
	if (op_req1 == 1'b1) 
	begin
			en_out1 <= 1'b1;
			pop_req1 <= 1'b1;
			ns1 <= state2_1;
			dcnt1<=1'b0;
	end
	else
	begin
			en_out1 <= 1'b0;
			pop_req1 <= 1'b0;
			ns1 <= state1_1;
			dcnt1<=1'b0;

	end
end

state2_1: begin
	en_out1<=1'b0;
	if (counter1 > 6'b0) 
	begin
		ns1<= state2_1;
		dcnt1 <= 1'b1;
		pop_req1<=1'b0;
	end
	else
	begin
		dcnt1<=1'b0;
		ns1<= state1_1;
		pop_req1<=1'b0;
	end
end

default:
begin
	en_out1 <= 1'b0;
	pop_req1 <= 1'b0;
	ns1 <= state1_1;
	dcnt1<=1'b0;
end
endcase
end

always@(sr2,op_req2, counter2)
begin
case(sr2)
state0_2: begin	
		en_out2 <= 1'b0;
		pop_req2 <= 1'b0;
		ns2 <= state1_2;
		dcnt2<=1'b0;
end

state1_2: begin
	if (op_req2 == 1'b1) 
	begin
			en_out2 <= 1'b1;
			pop_req2 <= 1'b1;
			ns2 <= state2_2;
			dcnt2<=1'b0;
	end
	else
	begin
			en_out2 <= 1'b0;
			pop_req2 <= 1'b0;
			ns2 <= state1_2;
			dcnt2<=1'b0;

	end
end

state2_2: begin
	en_out2<=1'b0;
	if (counter2 > 6'b0) 
	begin
	  pop_req2 <= 1'b0;
		ns2<= state2_2;
		dcnt2 <= 1'b1;
	end
	else
	begin
	  pop_req2 <= 1'b0;
		dcnt2<=1'b0;
		ns2<= state1_2;
	end
end

default:
begin
	en_out2 <= 1'b0;
	pop_req2 <= 1'b0;
	ns2 <= state1_2;
	dcnt2<=1'b0;
end
endcase
end

always@(sr3,op_req3, counter3)
begin
case(sr3)
state0_3: begin	
		en_out3 <= 1'b0;
		pop_req3 <= 1'b0;
		ns3 <= state1_3;
		dcnt3<=1'b0;
end

state1_3: begin
	if (op_req3 == 1'b1) 
	begin
			en_out3 <= 1'b1;
			pop_req3 <= 1'b1;
			ns3 <= state2_3;
			dcnt3<=1'b0;
	end
	else
	begin
			en_out3 <= 1'b0;
			pop_req3 <= 1'b0;
			ns3 <= state1_3;
			dcnt3<=1'b0;

	end
end

state2_3: begin
	en_out3<=1'b0;
	if (counter3 > 6'b0) 
	begin
		ns3<= state2_3;
		dcnt3 <= 1'b1;
		pop_req3<=1'b0;
	end
	else
	begin
		dcnt3<=1'b0;
		ns3<= state1_3;
		pop_req3<=1'b0;
	end
end

default:
begin
	en_out3 <= 1'b0;
	pop_req3 <= 1'b0;
	ns3 <= state1_3;
	dcnt3<=1'b0;
end
endcase
end

endmodule





