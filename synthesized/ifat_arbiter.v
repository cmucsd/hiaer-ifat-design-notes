`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:17:37 03/15/2010 
// Design Name: 
// Module Name:    ifat_arbiter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ifat_arbiter(
	
		clk, rst,
	
		if0_outp, if0_ack, if0_req,
		if1_outp, if1_ack, if1_req,
		if2_outp, if2_ack, if2_req,
		if3_outp, if3_ack, if3_req,
		
		dl_inp, dl_req, dl_ack
);

input 		clk, rst;
	
input		[10:0] if0_outp,if1_outp,if2_outp,if3_outp; 
input 	if0_req,if1_req,if2_req,if3_req; 
output  if0_ack, if1_ack, if2_ack, if3_ack;
		
output	[12:0] dl_inp;
output 	dl_req;
input  dl_ack;

reg if0_ack, if1_ack, if2_ack, if3_ack;
reg [12:0]idl_inp;
reg idl_req, idl_ack;
reg [12:0] dl_inp;
reg dl_req;


parameter /*synopsys enum state*/
state0 = 3'b000,
state1 = 3'b001,
state2 = 3'b010,
state3 = 3'b011,
state4 = 3'b100;
//synopsys state_vector state_reg
reg [2:0] state_reg, next_state;




always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
		state_reg = state0; 
	end  
   else  
	begin
		state_reg = next_state;
	end
end


always @ (posedge clk, posedge rst)
begin  
	if (rst == 1'b1) 
	begin
	idl_ack <= 1'b0;
	dl_req <= 1'b0;
	dl_inp <= 20'b0;
	end  
   else  
	begin
	idl_ack <= dl_ack;
	dl_req <= idl_req;
	dl_inp <= idl_inp;
	end
end


always @(state_reg, if0_outp, if1_outp, if2_outp, if3_outp, if0_req, if1_req, if2_req, if3_req,idl_ack)
begin
case (state_reg)
state0:// reset state 
begin
	idl_inp <= 21'b0;
	idl_req <= 1'b0;
	if0_ack <=1'b0;
	if1_ack <=1'b0;
	if2_ack <=1'b0;
	if3_ack <=1'b0;
	next_state <= state1;
end

state1:// check ifat-module 0 for data

if (if0_req == 1'b1)
begin
  if (idl_ack == 1'b0)
  begin 
  	idl_inp <= {2'b00,if0_outp};
  	idl_req <= 1'b1;
  	if0_ack <= idl_ack;
  	if1_ack <=1'b0;
  	if2_ack <=1'b0;
  	if3_ack <=1'b0;
  	next_state <= state1;
 	end
	else
	 begin
	 idl_inp <= {2'b00,if0_outp};
  	idl_req <= 1'b1;
  	if0_ack <= idl_ack;
  	if1_ack <=1'b0;
  	if2_ack <=1'b0;
  	if3_ack <=1'b0;
	next_state <= state1;
	end
end
else
begin
  if (idl_ack==1'b1)
  begin
		idl_inp <= {2'b00,if0_outp};
		idl_req <= 1'b0;
		if0_ack <=idl_ack;
		if1_ack <=1'b0;
		if2_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state1;
	end
	else
	begin
		idl_inp <= 21'b0;
		idl_req <= 1'b0;
		if0_ack <=idl_ack;
		if1_ack <=1'b0;
		if2_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state2;
	end
end

state2:// check ifat-module 1 for data

if (if1_req == 1'b1)
begin
  if (idl_ack == 1'b0)
  begin 
  	idl_inp <= {2'b01,if1_outp};
  	idl_req <= 1'b1;
  	if1_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if2_ack <=1'b0;
  	if3_ack <=1'b0;
  	next_state <= state2;
 	end
	else
	 begin
	 idl_inp <= {2'b01,if1_outp};
  	idl_req <= 1'b1;
  	if1_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if2_ack <=1'b0;
  	if3_ack <=1'b0;
	next_state <= state2;
	end
end
else
begin
  if (idl_ack==1'b1)
  begin
		idl_inp <= {2'b01,if1_outp};
		idl_req <= 1'b0;
		if1_ack <=idl_ack;
		if0_ack <=1'b0;
		if2_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state2;
	end
	else
	begin
		idl_inp <= 21'b0;
		idl_req <= 1'b0;
		if1_ack <=idl_ack;
		if0_ack <=1'b0;
		if2_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state3;
	end
end
state3:// check ifat-module 2 for data

if (if2_req == 1'b1)
begin
  if (idl_ack == 1'b0)
  begin 
  	idl_inp <= {2'b10,if2_outp};
  	idl_req <= 1'b1;
  	if2_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if1_ack <=1'b0;
  	if3_ack <=1'b0;
  	next_state <= state3;
 	end
	else
	 begin
	 idl_inp <= {2'b10,if2_outp};
  	idl_req <= 1'b1;
  	if2_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if1_ack <=1'b0;
  	if3_ack <=1'b0;
	next_state <= state3;
	end
end
else
begin
  if (idl_ack==1'b1)
  begin
		idl_inp <= {2'b10,if2_outp};
		idl_req <= 1'b0;
		if2_ack <=idl_ack;
		if0_ack <=1'b0;
		if1_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state3;
	end
	else
	begin
		idl_inp <= 21'b0;
		idl_req <= 1'b0;
		if1_ack <=idl_ack;
		if0_ack <=1'b0;
		if2_ack <=1'b0;
		if3_ack <=1'b0;
		next_state <= state4;
	end
end

state4:// check ifat-module 3 for data

if (if3_req == 1'b1)
begin
  if (idl_ack == 1'b0)
  begin 
  	idl_inp <= {2'b11,if3_outp};
  	idl_req <= 1'b1;
  	if3_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if1_ack <=1'b0;
  	if2_ack <=1'b0;
  	next_state <= state4;
 	end
	else
	 begin
	 idl_inp <= {2'b11,if3_outp};
  	idl_req <= 1'b1;
  	if3_ack <= idl_ack;
  	if0_ack <=1'b0;
  	if1_ack <=1'b0;
  	if2_ack <=1'b0;
	next_state <= state4;
	end
end
else
begin
  if (idl_ack==1'b1)
  begin
		idl_inp <= {2'b11,if3_outp};
		idl_req <= 1'b0;
		if3_ack <=idl_ack;
		if0_ack <=1'b0;
		if1_ack <=1'b0;
		if2_ack <=1'b0;
		next_state <= state4;
	end
	else
	begin
		idl_inp <= 21'b0;
		idl_req <= 1'b0;
		if3_ack <=1'b0;
		if0_ack <=1'b0;
		if2_ack <=1'b0;
		if1_ack <=1'b0;
		next_state <= state1;
	end
end

default :
begin
	idl_inp <= 21'b0;
	idl_req <= 1'b0;
	if0_ack <=1'b0;
	if1_ack <=1'b0;
	if2_ack <=1'b0;
	if3_ack <=1'b0;
	next_state <= state1;
end



endcase
end

endmodule

