`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    05:34:22 03/16/2010 
// Design Name: 
// Module Name:    top_port 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
// Decoding of ext_in
// [22:21] maps to wire address[1:0] and selects port (group of 2^13 neurons)
//   A port serves half a 3x3 tile (called IFAT).
// [20:19] selects group of 2^11 neurons within an IFAT.
// [18:6] 13 bit address (further broken down to rows, columns, 
//   compartment, reverse potential in Teddy's block)
// [5:0] strength 
//   (translates to number of clk cycles that if[x]_pulse[y] gets asserted)
//
// Decoding of ext_out
// [22:15] unused, zero  
// [14:13] identifies port
// [12:11] identifies 2^11 neuron group (IFAT)
// [10:0] address of spiking neuron
//
// handshake timings are shown in 
// /shares/Chips/Tezzaron/siddharth/analog_layer_timing_diagram
//
//////////////////////////////////////////////////////////////////////////////////
module IFAT_AER_connect 
(
	clk, rst, ext_IFAT, ext_HIAER, addr,
	
	if0_din0, if0_dout0, if0_req0, if0_ack0, if0_pulse0,
	if0_din1, if0_dout1, if0_req1, if0_ack1, if0_pulse1,
	if0_din2, if0_dout2, if0_req2, if0_ack2, if0_pulse2,
	if0_din3, if0_dout3, if0_req3, if0_ack3, if0_pulse3,
	
	if1_din0, if1_dout0, if1_req0, if1_ack0, if1_pulse0,
	if1_din1, if1_dout1, if1_req1, if1_ack1, if1_pulse1,
	if1_din2, if1_dout2, if1_req2, if1_ack2, if1_pulse2,
	if1_din3, if1_dout3, if1_req3, if1_ack3, if1_pulse3,

	
	IFAT_data_out_0, IFAT_req_out_0, ack_IFAT_0,
	data_IFAT_0, request_IFAT_0, ack_from_IFAT_0,
	
	IFAT_data_out_1, IFAT_req_out_1, ack_IFAT_1,
	data_IFAT_1, request_IFAT_1, ack_from_IFAT_1,


	ext_in, ext_inack, ext_inreq,
	ext_out, ext_outack, ext_outreq	
    );

	input clk, rst, 
            ext_IFAT,   // 
            ext_HIAER;  // 
	input addr;
	
	/* IFAT 0 */
	input	[10:0] if0_din0, if0_din1,if0_din2,if0_din3;
	output [12:0] if0_dout0, if0_dout1,if0_dout2,if0_dout3;
	input if0_req0, if0_req1,if0_req2, if0_req3;
	output if0_ack0, if0_pulse0, if0_ack1, if0_pulse1, if0_ack2, if0_pulse2,if0_ack3, if0_pulse3;

	/* IFAT 1 */
	input	[10:0] if1_din0, if1_din1,if1_din2,if1_din3;
	output [12:0] if1_dout0, if1_dout1,if1_dout2,if1_dout3;
	input if1_req0, if1_req1,if1_req2, if1_req3;
	output if1_ack0, if1_pulse0, if1_ack1, if1_pulse1, if1_ack2, if1_pulse2,if1_ack3, if1_pulse3;

	/* Port 0 */
	input [20:0] IFAT_data_out_0;
	input	IFAT_req_out_0;
	output ack_IFAT_0;
	output [12:0] data_IFAT_0;
	output	request_IFAT_0;
	input ack_from_IFAT_0;
	
	/* Port 1 */
	input [20:0] IFAT_data_out_1;
	input	IFAT_req_out_1;
	output ack_IFAT_1;
	output [12:0] data_IFAT_1;
	output 	request_IFAT_1;
	input ack_from_IFAT_1;



	input [22:0] ext_in;
	input ext_inreq;
	output ext_inack;
	output [22:0] ext_out;
	output ext_outreq;
	input ext_outack;


	reg [20:0] fifo_in_dl0, fifo_in_dl1;
	reg req_dl0,req_dl1;
	reg dl_ack0,dl_ack1;
	
	reg [12:0] data_IFAT_0,data_IFAT_1;
	reg request_IFAT_0,request_IFAT_1;
	reg ack_IFAT_0,ack_IFAT_1;

	
	reg [22:0] iext_out;
	reg iext_outreq;
	reg iext_inack;

	wire [12:0] dl_inp0, dl_inp1;
	wire [1:0] address;		
	wire [1:0] add1, add2;
	wire addr;
			
 ifat_arbiter ifarb0(.clk(clk), .rst(rst),
		.if0_outp(if0_din0), .if0_ack(if0_ack0), .if0_req(if0_req0),.if1_outp(if0_din1), .if1_ack(if0_ack1), .if1_req(if0_req1),
		.if2_outp(if0_din2), .if2_ack(if0_ack2), .if2_req(if0_req2), .if3_outp(if0_din3), .if3_ack(if0_ack3), .if3_req(if0_req3),
		.dl_inp(dl_inp0), .dl_req(dl_req0), .dl_ack(dl_ack0));


 input_to_al ifinp0(  .clk(clk), .rst(rst),  	 
    .if0_out_al(if0_dout0), .if0_en(if0_pulse0),  .if1_out_al(if0_dout1), .if1_en(if0_pulse1),
    .if2_out_al(if0_dout2), .if2_en(if0_pulse2),  .if3_out_al(if0_dout3), .if3_en(if0_pulse3),
	 .req_dl(req_dl0),.ack_dl(ack_dl0),.fifo_in_dl(fifo_in_dl0));
	 
	 
	 
  ifat_arbiter ifarb1(	.clk(clk), .rst(rst),
		.if0_outp(if1_din0), .if0_ack(if1_ack0), .if0_req(if1_req0),.if1_outp(if1_din1), .if1_ack(if1_ack1), .if1_req(if1_req1),
		.if2_outp(if1_din2), .if2_ack(if1_ack2), .if2_req(if1_req2), .if3_outp(if1_din3), .if3_ack(if1_ack3), .if3_req(if1_req3),
		.dl_inp(dl_inp1), .dl_req(dl_req1), .dl_ack(dl_ack1));


 input_to_al ifinp1(  .clk(clk), .rst(rst),  	 
    .if0_out_al(if1_dout0), .if0_en(if1_pulse0),  .if1_out_al(if1_dout1), .if1_en(if1_pulse1),
    .if2_out_al(if1_dout2), .if2_en(if1_pulse2),  .if3_out_al(if1_dout3), .if3_en(if1_pulse3),
	 .req_dl(req_dl1),.ack_dl(ack_dl1),.fifo_in_dl(fifo_in_dl1));

	 //assign ack_from_listn = (en_ack_from_listn) ? ack_BUS: 1'bZ;
	 assign add1 = {addr,1'b0};
	 assign add2 = {addr,1'b1};
	 assign address = ext_in[22:21];
	 assign ext_out = ((ext_IFAT||ext_HIAER) && ((address == add1)||(address == add2)))	 ? iext_out:23'bZ;
	 assign ext_outreq = ((ext_IFAT||ext_HIAER) && ((address == add1)||(address == add2)))? iext_outreq:1'bZ;
	 assign ext_inack = ((ext_IFAT||ext_HIAER) && ((address == add1)||(address == add2)))? iext_inack:1'bZ;
	 
	 
always @ (ext_IFAT, ext_HIAER ,address, IFAT_data_out_0, IFAT_data_out_1, IFAT_req_out_0, IFAT_req_out_1, dl_inp0,dl_req0,ack_dl1,dl_inp1,dl_req1,ack_from_IFAT_1,ack_from_IFAT_0,
ext_outack, ext_in, ext_inreq, ack_dl0,add1,add2)
begin
case ({ext_IFAT, ext_HIAER})
0:begin
	fifo_in_dl0 = IFAT_data_out_0;
	req_dl0 = IFAT_req_out_0;
	ack_IFAT_0 = ack_dl0;
	
	data_IFAT_0 = dl_inp0;
	request_IFAT_0 = dl_req0;
	dl_ack0 =	ack_from_IFAT_0;
	
	fifo_in_dl1 = IFAT_data_out_1;
	req_dl1 = IFAT_req_out_1;
	ack_IFAT_1 = ack_dl1;
	
	data_IFAT_1 = dl_inp1;
	request_IFAT_1 = dl_req1;
	dl_ack1 =	ack_from_IFAT_1;

	
	iext_out = 23'b0;
	iext_outreq = 1'b0;
	iext_inack = 1'b0;
	
end

1:begin// from the digital layer to the testunit - the IFAT gets its pins set to 0
	if (address == add1)
	begin
		iext_out = {address, IFAT_data_out_0}; 
		iext_outreq = IFAT_req_out_0;
		ack_IFAT_0 = ext_outack;
		
		data_IFAT_0 = ext_in[12:0];
		request_IFAT_0 = ext_inreq;
		iext_inack=	ack_from_IFAT_0;
		
		fifo_in_dl0 = 21'b0;
		req_dl0 = 1'b0;
		dl_ack0 = 1'b0;
		
		fifo_in_dl1 = 21'b0;
		req_dl1 = 1'b0;
		dl_ack1 = 1'b0;
		
		data_IFAT_1 =13'b0;
		request_IFAT_1 = 1'b0;
		ack_IFAT_1 =  1'b0;

	end
	else
	if (address == add2)
	begin
		iext_out = {address,IFAT_data_out_1}; 
		iext_outreq = IFAT_req_out_1;
		ack_IFAT_1 = ext_outack;
		
		data_IFAT_1 = ext_in[12:0];
		request_IFAT_1 = ext_inreq;
		iext_inack=	ack_from_IFAT_1;
		
		fifo_in_dl0 = 21'b0;
		req_dl0 = 1'b0;
		dl_ack0 = 1'b0;
		
		fifo_in_dl1 = 21'b0;
		req_dl1 = 1'b0;
		dl_ack1 = 1'b0;
		
		data_IFAT_0 = 13'b0;
		request_IFAT_0 = 1'b0;
		ack_IFAT_0 =  1'b0;


	end
	else
	begin
		iext_out = 23'b0;
		iext_outreq = 1'b0;
		iext_inack = 1'b0;

		fifo_in_dl0 = 21'b0;
		req_dl0 = 1'b0;
		dl_ack0 = 1'b0;
		
		data_IFAT_0 = 13'b0;
		request_IFAT_0 = 1'b0;
		ack_IFAT_0 =  1'b0;
					
		fifo_in_dl1 = 21'b0;
		req_dl1 = 1'b0;
		dl_ack1 = 1'b0;
			
		data_IFAT_1 = 13'b0;
		request_IFAT_1 = 1'b0;
		ack_IFAT_1 =  1'b0;

	end
end

2:begin // from the IFAT to the test unit the 
	// - the AER module gets its pins set to 0
	// With only the IFAT chips, this is the mode to use


	if (address == add1)
	begin
		fifo_in_dl0 = ext_in[22:0];
		req_dl0 = ext_inreq;
		iext_inack = ack_dl0;
		
		iext_out = {address,dl_inp0};
		iext_outreq = dl_req0;
		dl_ack0 =	ext_outack;
		
		data_IFAT_0 = 13'b0;
		request_IFAT_0 = 1'b0;
		ack_IFAT_0 =  1'b0;
				
		fifo_in_dl1 = 21'b0;
		req_dl1 = 1'b0;
		dl_ack1 = 1'b0;
		
		data_IFAT_1 = 13'b0;
		request_IFAT_1 = 1'b0;
		ack_IFAT_1 =  1'b0;
	end
	else
	begin
	if (address == add2)
	begin
		fifo_in_dl1 = ext_in[22:0];
		req_dl1 = ext_inreq;
		iext_inack = ack_dl1;
		
		iext_out = {address,dl_inp1};
		iext_outreq = dl_req1;
		dl_ack1 =	ext_outack;
		
		data_IFAT_0 = 13'b0;
		request_IFAT_0 = 1'b0;
		ack_IFAT_0 =  1'b0;
				
		fifo_in_dl0 = 21'b0;
		req_dl0 = 1'b0;
		dl_ack0 = 1'b0;
		
		data_IFAT_1 = 13'b0;
		request_IFAT_1 = 1'b0;
		ack_IFAT_1 =  1'b0;

	end
	else
	begin
		iext_out = 23'b0;
		iext_outreq = 1'b0;
		iext_inack = 1'b0;

		fifo_in_dl0 = 21'b0;
		req_dl0 = 1'b0;
		dl_ack0 = 1'b0;
		
		data_IFAT_0 = 13'b0;
		request_IFAT_0 = 1'b0;
		ack_IFAT_0 =  1'b0;
					
		fifo_in_dl1 = 21'b0;
		req_dl1 = 1'b0;
		dl_ack1 = 1'b0;
			
		data_IFAT_1 = 13'b0;
		request_IFAT_1 = 1'b0;
		ack_IFAT_1 =  1'b0;

	end
	end
end
 
3:begin		// from the testunit to the testunit then 
		// - everything else gets its pins set to 0
		// and the external ports are shorted
	iext_out = ext_in;
	iext_outreq = ext_inreq;
	iext_inack = ext_outack;

	fifo_in_dl0 = 21'b0;
	req_dl0 = 1'b0;
	dl_ack0 = 1'b0;
	
	data_IFAT_0 = 13'b0;
	request_IFAT_0 = 1'b0;
	ack_IFAT_0 =  1'b0;
				
	fifo_in_dl1 = 21'b0;
	req_dl1 = 1'b0;
	dl_ack1 = 1'b0;
		
	data_IFAT_1 = 13'b0;
	request_IFAT_1 = 1'b0;
	ack_IFAT_1 =  1'b0;


end
endcase
end 
endmodule




