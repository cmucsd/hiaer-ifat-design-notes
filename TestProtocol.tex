\documentclass[10pt,onecolumn]{article}
\usepackage{color}
\usepackage{verbatim}
%
\def\volt{\,\mathrm{V}}
\def\mV{\,\mathrm{mV}}
\def\ohm{\,\Omega}
\def\MHz{\,\mathrm{MHz}}
\def\kiloohm{\,\mathrm{k}\Omega}
\def\uF{\,\mu\mathrm{F}}
\def\Vcco2{V_\mathrm{CCo2}}
\def\warning#1{\textcolor{red}{#1}}

%
\begin{document}
\title{{\bf HiAER--IFAT test board: Test Protocol\/}}
\author{Christoph Maier, Jongkil Park, Theodore Yu, Srinjoy Das}
\date{October~5,~2011--\today}
\maketitle
%%
\section{Initial power-up}
%%
\subsection{Power supplies}
{\sl Christoph Maier, Jongkil Park, 05--06~October~2011\/}

Using the BK~1670 power supply, I applied 5.0\,V power to {\sf J2\/}, slowly ramping up the current limit.
Initially, the {\sf Power Good\/} LEDs flicker, until they remain off except for the {\sf 2.5\,V good\/} LED.
In one operating mode, the current stabilizes at 1.00\,A, and the {\sf DONE\/} LED lights up.

After further turning up the current limit of the 5\,V power supply, 
the current consumption stabilizes at 1.33\,A, 
with all the {\sf Power Good\/} LEDs and all the $\overline\mathsf{INIT}$ LEDs lit up.

Measured the {sf MAX\,8686\/} power supplies at the {\sf C191\/} capacitors:
\begin{itemize}
\item 2.5\,V good
\item 1.2\,V good
\item 1.5\,V good
\item 1.8\,V good
\end{itemize}
Measured 
\begin{itemize}
\item $1.5\volt_\mathrm{analog}$ at {\sf C173\/}---good.
\item $5\volt_\mathrm{analog}$ at {\sf C178\/}---good.
\item VTTDDR: $0.75\volt$ at {\sf U3 pin~5\/}---good after all FPGAs are programmed.
\item VTTVREF: $0.75\volt$ at {\sf U3 pin~6\/}---good after all FPGAs are programmed.
\item $\Vcco2$: $1.8\volt$ at power-up, changes to $1.5\volt$ after all FPGAs are programmed---good.
\end{itemize}

\subsubsection{Resolving power supply instability}
%
{\sl Observed error modes\/}

{\it As of 10~October~2011, the $1.5\volt$ and $1.8\volt$ supplies turn off, and the dual MOSFET {\sf U30\/} heats up extremely,
but only if the 5\,V~6\,A switch mode power supply is used. Bypassing {\sf U29\/} with 10\,nF doesn't help.
The {\sf VCC5V\/} line, measured with an oscilloscope, looks quite noisy: 
an oscillation with a frequency of some~8\,kHz and some $\pm40\mV$ amplitude.
Disconnecting (actually, irreparably tearing off) the $220\uF$ bypass capacitor at the input doesn't make a difference, either,
nor does it change the {\sf VCC5V\/} oscillation.

Tested the board with the R.S.R.\,3033 laboratory power supply, in parallel (max.~6\,A) source mode. 
{\sf U30\/}~does not heat up; 
all {\sf MAX8686\/} generated powers remain good.\/}

{\noindent\bf Solution to power supply instability\/}

{\sl Christoph Maier, 28~October~2011\/}

Looked at {\sf VCC5V\/} line with an oscilloscope and noticed that the ripple looks similar to the switching waveforms 
at the {\sf LX\/} terminals of the {\sf MAX8686\/}~\cite{MAX8686} voltage regulators. 
This indicates that the supply-side bypass capacitors for the regulators 
{\sf C184\/}, {\sf C186\/}, {\sf C187\/}, and {\sf C189\/} with a total capacitance of $20.2\uF$ 
are severely underdimensioned.
As a quick fix, I soldered $100\uF$ $6.3\volt$ X5R capacitors (Digi-Key~{\sf 587-1963-1-ND\/}) in parallel.
This substantially reduces the ripple on the {\sf VCC5V\/} line 
and eliminates the failure mode with the 5\,V~6\,A switch mode power supply.

{\noindent\bf Root cause of power supply instability\/}

{\sl Christoph Maier, August~2012\/}

When the {\sf MAX8686\/}~\cite{MAX8686} voltage regulators detect overvoltage at their output, they shunt it to {\sf GND\/}.
When switching the FPGA bank connected to both the programming FLASH and the IFAT ASIC from $1.8\volt$ to $1.5\volt$, 
the load of the $1.8\volt$ regulator becomes substantially less, 
an overvoltage occurs that causes the $1.8\volt$ supply to be shunted to {\sf GND}, 
which subsequently forward biases the diode in the $1.8\volt$ supply MOSFET switch 
and pulls down the $1.5\volt$ supply, too.

\subsection{Clock generation}
%
\subsubsection{Crystal oscillators}
{\sl Christoph Maier, 05~October~2011\/}

Measured Kyocera $200\MHz$ clock oscillators, pins 4~and~5, with Agilent DSO6034A oscilloscope and 10073C probes.
All oscillators produce a $200\MHz$ differential signal. 
The AC amplitude of each branch is some $\pm100\mV$. 
Probing substantially distorts the clock signals.

\subsubsection{Master clock distribution network}
{\sl Christoph Maier, 05~October~2011\/}

Measured the differential outputs of {\sf U59\/} with Agilent DSO6034A oscilloscope and 10073C probes.
The signals are hard to probe without magnifying visor; I occasionally shorted the $2.5\volt$ supply while measuring.
With no jumpers set (on-chip crystal oscillator {\sf U60\/} selected as source), 
$200\MHz$ differential signals are produced; 
they are affected by probing and have a higher amplitude than the crystal oscillator signals.

Confirmed distribution of $200\MHz$ master clock after FPGA programming.

Disable jumper for oscillator {\sf U60\/} works.

Disable jumper for clock distribution IC {\sf U59\/} works.

\subsubsection{External SMA clock inputs}
{\sl Christoph Maier, Jongkil Park, 05--06~October~2011\/}

Local SMA clock inputs: work with $20\MHz$ input, square wave, $V_\mathrm{low}=0.1\volt$, $V_\mathrm{low}=2.2\volt$, 
from Agilent~33220A with $50\ohm$ output impedance.

Global SMA clock input: 
\warning{SMA master clock input does {\bf not\/} seem to work 
with $20\MHz$ input, square wave, $V_\mathrm{low}=0.1\volt$, $V_\mathrm{low}=2.2\volt$, 
from Agilent~33220A with $50\ohm$ output impedance.}

\section{Programming}
%%
\subsection{JTAG communication}
{\sl Christoph Maier, Jongkil Park, 05--06~October~2011\/}

Testing JTAG scan chains with Xilinx Platform Cable USB II.
{\bf All\/} JTAG scan chains:
\begin{itemize}
\item Both FPGA and Cereal EEPROM are recognized
\item FPGAs are programmable through JTAG
\item Only after all FPGAs are programmed, 
the last programmed FPGA starts executing, and the {\sf DONE\/} LED lights up.
\item First program ({\tt blinkdit.bit\/}, see Appendix~\ref{app:blinkdit}) 
divides both local and global $200\MHz$ on-board differential clocks
and external SMA clock, and blinks status LEDs accordingly. 
\end{itemize}

\subsection{Start-up from serial flash}
%
{\sl Christoph Maier, 06~October~2011\/}

Using ISE Impact, I created a file {\tt blinkenlight\_v1.mcs\/} 
from the code shown in Appendix~\ref{app:blinkdit} 
and successfully programmed the all the Platform FLASH devices with it.

On power-up, all FPGAs start executing the {\tt blinkenlight\/} code, 
and $\Vcco2$ switches from $1.8\volt$ to $1.5\volt$ automatically.

\section{USB communication}
%%
{\sl Christoph Maier, 21~October~2011\/}

Created a simple program {\tt USBechoTest\_v0.mcs\/} that echoes any character received through an USB port immediately,
and programmed all Level~0 FPGA Platform Flash devices with it.

The Level~1 FPGA needs a special program {\tt USBechoTest\_v1.mcs\/} to accomodate the different pinout of the USB interface.

\noindent\warning{\sl Jongkil Park, 31~October~2011\/}

\warning{Trying to communicate through USB on system level gives errors: some bytes are sent twice!}

\section{DRAM access}
%%
{\sl Jongkil Park, 11~October~2011\/}

With a binary file {\tt one\_chip.bit\/} that instantiates both DDR3 controllers, 
the {\tt calib\_done} signals of both DRAM controllers properly asserted and the clock PLLs work as designed.
Current consumption increases from 1.4\,A to 1.8\,A if one FPGA is reprogrammed, 
an increase in power consumption by 2\,W.
\warning{This has not been checked with all FPGAs yet. With all {\sf L0\/} FPGA DRAM controllers instantiated, 
current consumption is 3.05\,A at 5\,V, for a total power consumption of some 15\,W.}

\section{FPGA-to-FPGA communication}
%%
\subsection{Parallel port}
%
\warning{tbd}

\subsection{Serial port}
%
\warning{tbd}

\section{Analog IFAT interface}
%%
\subsection{DACs}

{\sl Theodore Yu, 31~October~2011\/}

Created several functions to facilitate loading of analog DAC values onto the DAC chips on-board:

\warning{Please insert a reference to the file names, and a {\sf\textbackslash ref\/} to the Appendices.}

\begin{itemize}
\item Stored array of analog values for DACs with initial settings for event in directly leading to and event out
\item Generic update function to update $n$ analog DAC values into the array stored on chip. The function expects 
    \begin{itemize} 
    \item a single value indicating the number of entries +1 (this can be revised) to be updated.
    \item For each entry, a packet of 4 values specifying 
        \begin{itemize}
        \item the DAC chip number $[0\ldots4]$,
        \item address of DAC on the DAC chip $[0\ldots7]$, 
        \item MSB byte data $[0\ldots255]$, and 
        \item LSB byte data $[0\ldots255]$.
        \end{itemize}
        For example, 
        $$\mathsf{fwrite}[2\quad1\;4\;27\;19\quad4\;4\;11\;9\quad0\;0\;0\;0]$$
        updates \warning{four [3?]} entries in the DAC analog values stored on chip.
    \end{itemize}
\item Batch update function to update 5 analog DAC values into the array stored on chip.  The function expects a sequence of 5 packets of data indicating 
    \begin{itemize} 
    \item the address of DAC on the DAC chip $[0\ldots7]$, 
    \item MSB byte data $[0\ldots255]$, and 
    \item LSB byte data $[0\ldots255]$.
    \end{itemize}
\item Batch load function to load an analog DAC value from each DAC chip onto the board.  The function expects a sequence of 5 packets of data indicating 
    \begin{itemize} 
    \item the address of DAC on the DAC chip $[0\ldots7]$, 
    as it increments from DAC chip $0\ldots4$ in loading analog DAC values.
    \end{itemize}
    Then the function enables the DAC write module while going into wait state (this can be revised).  
    The DAC write module loads the daisychained data for the 5 DACs and then sends an acknowledge signal back to the main loop to indicate DONE.
\end{itemize}

\noindent{\sl Christoph Maier, 3~October~2012\/}

Wrote a stand-alone Verilog module {\tt dac.v\/} to write 16-bit data from an
addressable internal memory in the FPGA to the DACs. 
The module and associated test programs are described in
Appendix~\ref{app:dac}.
The module {\tt clk\_divider\/} in {\tt dac.v\/} (see \ref{app:dac.v}) 
needs to be 4~bits wide to divide the serial clock 
into the {\sf LTC\,2600\/} DAC daisy chain sufficiently.


\subsection{ADCs}
%
\warning{tbd}

\section{IFAT interfacing}

Upon testing, we noticed that external input 12 in the top half ExtIn12top of the IFAT chip was ``stuck" high whenever it was not being driven.  

Measurements with the chip inserted and jumpers connecting analog and digital power supplies of 1.5V 
resulted in 1V measured at ExtIn12top and 0.35V measured at other pins.  

Measurements with the chip not inserted and jumpers connecting analog and digital power supplies of 1.5V 
resulted in 0V measured at ExtIn12top and 0V measured at other pins.  

Measurements with the chip inserted and no jumpers connecting analog and digital power supplies of 1.5V 
resulted in 0.35V measured at ExtIn12top and 0.02V measured at other pins. 

\subsection{Systematic test of IFAT interface}
%
{\sl Christoph Maier, Srinjoy Das, 07~August~2012--20~February~2013\/}

Quite a bit of testing of the IFAT interface has been done; 
however, the Verilog code communicating with the IFAT AER ports 
is deeply buried in one huge state machine 
that incurs substantial worst-case latency.

This is a serious problem 
because the internal FIFOs in the IFAT chip freeze up if full.

We need to build {\em self-contained, testable\/} state machines 
that communicate to the IFAT AER ports.

The first step of the design needs to be 
an interface definition and a test protocol.

A likely candidate for the interface is Jongkil's {\tt src/Input\_reg\_file/Priority\_FIFO.v\/}, 
because we'll need to time synaptic events transmitted to the IFAT array, anyhow, 
to avoid freeze-ups due to FIFO overflow.

Because the HiAER and DRAM layers of the Tezzaron die stack didn't happen, 
we use auxiliary ports to access the IFAT die with the analog neurons and some arbitration logic directly.
The signal names \warning{(in the Altium schematic)} of the 23--bit ports are 
{\sf EXT\_IN\_BOTTOM\/}, {\sf EXT\_IN\_TOP\/}, {\sf EXT\_OUT\_BOTTOM\/}, {\sf EXT\_OUT\_TOP\/};
the handshake lines are 
{\sf EXT\_INREQ\_BOTTOM\/}, {\sf EXT\_INACK\_BOTTOM\/},
{\sf EXT\_INREQ\_TOP\/}, {\sf EXT\_INACK\_TOP\/},
{\sf EXT\_OUTREQ\_BOTTOM\/}, {\sf EXT\_OUTACK\_BOTTOM\/},
{\sf EXT\_OUTREQ\_TOP\/}, {\sf EXT\_OUTACK\_TOP\/}.
The auxiliary ports were never designed to drive off-chip lines, and are therefore very slow.

Internally, each $2^{11}$~neuron block has a FIFO and a pulse generator 
that creates a pulse of $w$~{\sf IFAT\_clk\/} cycles length, 
where $w$~is the synaptic weight as specified in the address event to the IFAT ASIC.

For the output of the IFAT ASIC, the neuron blocks are grouped in units of $4\times2^{11}=2^{13}$~neurons.
Each output block can only request transmission of axonal events 
if it has been selected at the input side of the neuronal array.
\warning{Add explanation of address bits, reference to Siddharth's source code.}

A test bench simulating address events sent to the IFAT (Section~\ref{tb_top_fpga_to_ifat_fifo_fsm.v}) shows 
that the internal input queue (Section~\ref{input_to_al.v}) locks up if there is more than 1~event in the queue.

A test bench simulating outgoing address events from the IFAT arrays 
(Sections \ref{tb_top_ifat_to_fpga_arbiter.v}
shows no obvious freeze-ups. However, the test bench does not quite accurately
reflect the connection of two {\tt IFAT\_AER\_connect\/}
(Section~\ref{IFAT_AER_connect.v}) modules to the
digital IFAT ASIC ports. 

The test bench {\tt tb\_ifat\_event\/} (Section~\ref{tb_ifat_event.v}) 
combines both incoming and outgoing AER streams from and to one IFAT ASIC
port. 
The logic that interfaces the IFAT ASIC digital ports is contained 
in the module {\tt IFAT\_interface\/} (Section~\ref{IFAT_interface.v}). 
This module should evolve into logic that can be physically implemented on the FPGA.

The address events are generated by the modules 
{\tt IFATarraySim\/} (Section~\ref{IFATarraySim.v}) and
{\tt AERstreamSim\/} (Section~\ref{AERstreamSim.v}).

\section{LVDS off-chip communication}
%%
\warning{It should be possible to connect Daniel Fasnacht's AEXv4 board to the LVDS ports 
after some changes in the termination networks.}

\section{Parallel bus off-chip communication}
%%
\warning{tbd}

%%%%
%
\begin{thebibliography}{999}
%
\bibitem{UG380ConfSeq}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, 
``Configuration Sequence'', p.~74
%
\bibitem{UG380ConfVoltages}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Table 5--11,
``Power Supplies Required for Configuration'', p.~75
%
\bibitem{UG380Startup}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Table 5--16,
``Default BitGen Sequence of Startup Events'', p.~80
%
\bibitem{UG380MultiBoot}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Chapter~7,
``Reconfiguration and MultiBoot'', pp.~123\,ff.
%
\bibitem{UG380ch2}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Chapter~2, pp.~22\,ff.
%
\bibitem{UG380p13}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Chapter~1, 
``Configuration Overview'', p.~13
%
\bibitem{UG380fig2-2}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Figure~2--2, p.~24
%
\bibitem{UG380p52}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, 
``Board Layout for Configuration Clock (CCLK)'', p.~52
%
\bibitem{UG380p65}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, 
``Configuration Details'', p.~65
%
\bibitem{UG380p66}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Table~5--2,
``Spartan-6 FPGA Configuration Pin Termination'', p.~66
%
\bibitem{UG380p55}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, chapter~3,
``Boundary-Scan and JTAG Configuration'', p.~55\,ff.
%
\bibitem{UG380JTAG}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, chapter~10,
``Advanced JTAG Configurations'', p.~145\,ff.
%
\bibitem{UG380JTAGinstr}
Xilinx document {\bf UG380\/} v2.2, 
{\sl Spartan-6 FPGA Configuration User Guide\/}, Table~10--2,
``Spartan-6 FPGA Boundary-Scan Instructions'', p.~150
%
\bibitem{XCF16data}
Xilinx document {\bf DS123\/} v2.18, 
{\sl Platform Flash In-System Programmable Configuration PROMs\/}, May~19, 2010
%
\bibitem{UG161ConfSetup}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, Figure~2--1, 
``Basic FPGA Master Serial Configuration Setup'', p.~24, October~26, 2009
%
\bibitem{UG161PinConnect}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, Table~2--1, 
``Pin Connections for Basic FPGA Master Serial Mode'', p.~24, October~26, 2009
%
\bibitem{UG161DesRev}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, Chapter~3,
``XCFxxP Design Revisions'', pp.~29\,ff., October~26, 2009
%
\bibitem{UG161reset}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, 
``Reset and Power On Reset Activation'', p.~93, October~26, 2009
%
\bibitem{UG161jtagCF}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, 
``PROM JTAG Boundary-Scan Chain'', p.~94, October~26, 2009
%
\bibitem{UG161doneLED}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/}, Figure 7--2, 
``Driving an LED with the DONE Pin'', p.~95, October~26, 2009
%
\bibitem{UG161resetCF}
Xilinx document {\bf UG161\/} v1.5,
{\sl Platform Flash PROM User Guide\/},  
``Using the PROM CF Pin to Initiate FPGA Configuration'', p.~95, October~26, 2009
%
\bibitem{MAX8686}
MAX8686: ``Single/Multiphase, Step-Down, DC-DC Converter Delivers Up to 25A Per Phase'', 
data sheet {\sl 19--4113; Rev~1; 10/10\/}; 
http://datasheets.maxim-ic.com/en/ds/MAX8686.pdf
%
\end{thebibliography}

%%%%
\pagebreak
\appendix
\section*{Source code}
\section{Digital logic on the IFAT ASIC}\label{app:IFAT_digital}
%%
Verilog code of the digital logic interfacing the analog neuron arrays with the digital I/O ports

\subsection{Verilog source {\tt IFAT\_AER\_connect.v\/}}\label{IFAT_AER_connect.v}
%
Top level module connecting the analog IFAT arrays to the digital ports of the IFAT ASIC.

\subsubsection{Edited for readability}
%
{\scriptsize\verbatiminput{IFAT_sim/IFAT_AER_connect.v}}

\subsubsection{Original synthesized version}
%
{\scriptsize\verbatiminput{synthesized/IFAT_AER_connect.v}}

\subsection{Verilog source {\tt ifat\_arbiter.v\/}}\label{ifat_arbiter.v}
%
\subsubsection{Edited for readability}
%
{\scriptsize\verbatiminput{IFAT_sim/ifat_arbiter.v}}

\subsubsection{Original synthesized version}
%
{\scriptsize\verbatiminput{synthesized/ifat_arbiter.v}}

\subsection{Verilog source {\tt input\_to\_al.v\/}}\label{input_to_al.v}
%
Module distributing address events into the IFAT neuron array to the synapses.
This module generates the excitatory pulses.
\subsubsection{Edited for readability}
%
{\scriptsize\verbatiminput{IFAT_sim/input_to_al.v}}

\subsubsection{Original synthesized version}
%
{\scriptsize\verbatiminput{synthesized/input_to_al.v}}

\subsection{Verilog source {\tt queue\_backend.v}}\label{queue_backend.v}
%
Priority queue factored out from \ref{input_to_al.v}, {\tt input\_to\_al.v\/}.
{\scriptsize\verbatiminput{IFAT_sim/queue_backend.v}}


\subsection{Verilog source {\tt Priority\_buffer.v\/}}\label{Priority_buffer.v}
%
Priority buffer, used in \ref{queue_backend.v}, {\tt queue\_backend.v}
in the refactored code, 
and in \ref{input_to_al.v}, {\tt input\_to\_al.v\/}
in the original synthesized code.

\subsubsection{Edited for readability}
%
{\scriptsize\verbatiminput{IFAT_sim/Priority_buffer.v}}

\subsubsection{Original synthesized version}
%
{\scriptsize\verbatiminput{synthesized/Priority_buffer.v}}

\subsection{Verilog source {\tt reg22x16.v\/}}\label{reg22x16.v}
%
$16\times22$~bit register file used in \ref{Priority_buffer.v}, {\tt Priority\_buffer.v\/}.
{\scriptsize\verbatiminput{synthesized/reg22x16.v}}

\subsection{Verilog source {\tt register\_22bits.v\/}}\label{register_22bits.v}
%
22~bit wide register file comprising Chartered Semiconductor 130\,nm CMOS standard cells {\tt RF2R1WX1TL\/},
used in \ref{reg22x16.v}, {\tt reg22x16.v\/}.
{\scriptsize\verbatiminput{synthesized/register_22bits.v}}

\section{Test benches for the digital logic in the IFAT ASIC}\label{app:test_digital_asic}
%%
\subsection{Verilog source {\tt tb\_top\_fpga\_to\_ifat\_fifo\_fsm.v}}\label{tb_top_fpga_to_ifat_fifo_fsm.v}
%
Srinjoy's test bench for signals from IFAT neuron array to FPGA.
The test bench shows how the internal {\tt input\_to\_al\/} (section~\ref{input_to_al.v}) module freezes up 
if there is more than one entry in the internal {\tt Priority\_buffer\/}.

{\scriptsize\verbatiminput{IFAT_sim/tb_top_fpga_to_ifat_fifo_fsm.v}}

\subsection{Verilog source {\tt tb\_top\_ifat\_to\_fpga\_arbiter.v}}\label{tb_top_ifat_to_fpga_arbiter.v}
%
Srinjoy's test bench for signals from IFAT neuron array to FPGA. 
There are some inconsistencies in the test bench.
{\scriptsize\verbatiminput{IFAT_sim/tb_top_ifat_to_fpga_arbiter.v}}

\subsection{Verilog source {\tt tb\_ifat\_event.v}}\label{tb_ifat_event.v}
%
Christoph's test bench for signals from IFAT neuron array to FPGA and from
FPGA to IFAT synapses. 
This test bench correctly uses two {\tt IFAT\_AER\_connect.v\/} modules (section~\ref{IFAT_AER_connect.v}), 
16~{\tt IFATarraySim.v\/} modules (Section~\ref{IFATarraySim.v}) to simulate
output address events from the analog neurons, 
and a~{\tt IFAT\_interface.v\/} module (Section~\ref{IFAT_interface.v}) 
containing the IFAT interface control logic in the FPGA. 
At the moment, {\tt IFAT\_interface.v\/} contains an incoming AER stream
simulator, {\tt AERstreamSim.v\/} (Section~\ref{AERstreamSim.v}).

{\scriptsize\verbatiminput{IFAT_sim/tb_ifat_event.v}}

\subsection{Verilog source {\tt IFATarraySim.v\/}}\label{IFATarraySim.v}
%
Parametric module in the {\tt tb\_ifat\_event.v} test bench (Section~\ref{tb_ifat_event.v}) 
simulating a stream of AER outputs from one $2^{11}$~neuron array.

\noindent The fully asynchronous module has 5~parameters.
\begin{description}
\item[\tt delay\/] between falling edge of {\tt ack\/} and a new spike address,
\item[\tt setup\/] between new spike address and rising edge of {\tt req\/},
\item[\tt hold\/] between rising edge of {\tt ack\/} and falling edge of {\tt req\/},
\item[\tt block\/] is displayed in diagnostic messages to distinguish between neuron blocks,
\item[\tt seed\/] is the initial value of the bitreverse address generator.
\end{description}
{\scriptsize\verbatiminput{IFAT_sim/IFATarraySim.v}}

\subsection{Verilog source {\tt IFAT\_interface.v\/}}\label{IFAT_interface.v}
%
Module in the {\tt tb\_ifat\_event.v} test bench (Section~\ref{tb_ifat_event.v})
containing the FPGA state machine connecting to one of the 2~IFAT digital
ports.
At the moment, this module contains a simulated AER stream generator, {\tt
  AERstreamSim\/} (Section~\ref{AERstreamSim.v}).
{\scriptsize\verbatiminput{IFAT_sim/IFAT_interface.v}}

\subsection{Verilog source {\tt AERstreamSim.v}}\label{AERstreamSim.v}
%
Parametric module that generates a 31-bit AER stream.

\noindent The fully asynchronous module has 5~parameters.
\begin{description}
\item[\tt delay\/] between falling edge of {\tt acknowledge\/} and a new spike address,
\item[\tt setup\/] between new spike address and rising edge of {\tt request\/},
\item[\tt hold\/] between rising edge of {\tt acknowledge\/} and falling edge of {\tt request\/},
\item[\tt mode\/] is a placeholder to allow generation of different AER streams,
\item[\tt seed\/] is the initial value of the bitreverse address generator.
\end{description}
{\scriptsize\verbatiminput{IFAT_sim/AERstreamSim.v}}


\section{Test program {\tt blinkdit\/}}\label{app:blinkdit}
%%
This program divides the differential clock inputs to the FPGAs by~$10^8$ 
and uses the indicator LEDs of one color as 2~bit binary counters. 
For a clock frequency of $200\MHz$, this results in 1~count/second.

\subsection{Verilog source {\tt blinkdit.v}}\label{app:blinkdit.v}
%
{\scriptsize\verbatiminput{blinkdit.v}}

\subsection{FPGA I/O assignment {\tt blinkdit.ucf}}\label{app:blinkdit.ucf}
%
I/O pin assignment for Verilog program {\tt blinkdit.v} (see Appendix~\ref{app:blinkdit.v})
{\scriptsize\verbatiminput{blinkdit.ucf}}

\section{Test program {\tt USB\_top\/}}\label{app:USB_top}
%%
This program divides the differential clock inputs to the FPGAs by~$5\cdot10^7$ 
and blinks 3 indicator LEDs. 
It echos characters received through the USB port and shows the status of the {\sf FT245R\/} USB interface lines
with the other LEDs.

\subsection{Verilog source {\tt USB\_top.v}}\label{app:USB_top.v}
%
{\scriptsize\verbatiminput{USB_top.v}}

\subsection{FPGA I/O assignment {\tt USB\_top.ucf}}\label{app:USB_top.ucf}
%
I/O pin assignment for Verilog program {\tt USB\_top.v} (see Appendix~\ref{app:USB_top.v}), Level~0 FPGA version
{\scriptsize\verbatiminput{USB_top.ucf}}

\subsection{FPGA I/O assignment {\tt USB\_L1.ucf}}\label{app:USB_L1.ucf}
%
I/O pin assignment for Verilog program {\tt USB\_top.v} (see Appendix~\ref{app:USB_top.v}), Level~1 FPGA version
{\scriptsize\verbatiminput{USB_L1.ucf}}

\section{Test program and module to drive IFAT bias DACs}\label{app:dac}
%%
The module {\tt dac.v\/} (see Appendix~\ref{app:dac.v}) copies $5\times8$
16-bit DAC bias settings from a memory array in the FPGA to the DACs biasing
the IFAT.

{\tt dac\_wrapper.v\/} (see Appendix~\ref{app:dac_wrapper.v}) 
is a test configuration to program the DACs; 

{\tt test\_dac.v\/} (see Appendix~\ref{app:test_dac.v}) 
is a test module for simulation.

{\tt test\_shift\_sequencer.v\/}, 
{\tt test\_data\_read.v\/}, 
and {\tt test\_clk\_divider.v\/} 
are unit tests for modules within {\tt dac.v\/}.

\subsection{Verilog source {\tt dac.v}}\label{app:dac.v}
%
Verilog module to write bias voltage coefficients from internal FPGA memory to
the DACs biasing the IFAT array.
{\scriptsize\verbatiminput{dac.v}}

\subsection{Verilog source {\tt dac\_wrapper.v}}\label{app:dac_wrapper.v}
%
Top Verilog module to embed {\tt dac.v\/} (see Appendix~\ref{app:dac.v}) into FPGA for testing.
{\scriptsize\verbatiminput{dac_wrapper.v}}

\subsection{FPGA I/O assignment {\tt dac.ucf}}\label{app:dac.ucf}
%
I/O pin assignment for Verilog program {\tt dac\_wrapper.v\/} 
(see Appendix~\ref{app:dac_wrapper.v})
{\scriptsize\verbatiminput{dac.ucf}}

\subsection{Verilog source {\tt test\_dac.v}}\label{app:test_dac.v}
%
Verilog test module for {\tt dac.v\/} (see Appendix~\ref{app:dac.v}).
{\scriptsize\verbatiminput{test_dac.v}}

\subsection{Verilog source {\tt test\_shift\_sequencer.v}}\label{app:test_shift_sequencer.v}
%
Verilog test module for {\tt shift\_sequencer\/} module in file {\tt dac.v\/} (see Appendix~\ref{app:dac.v}).
{\scriptsize\verbatiminput{test_shift_sequencer.v}}

\subsection{Verilog source {\tt test\_data\_read.v}}\label{app:test_data_read.v}
%
Verilog test module for {\tt test\_data\_read\/} module in file {\tt dac.v\/} (see Appendix~\ref{app:dac.v}).
{\scriptsize\verbatiminput{test_data_read.v}}

\subsection{Verilog source {\tt test\_clk\_divider.v}}\label{app:test_clk_divider.v}
%
Verilog test module for {\tt clk\_divider\/} module in file {\tt dac.v\/} (see Appendix~\ref{app:dac.v}).
{\scriptsize\verbatiminput{test_clk_divider.v}}

\subsection{DAC parameter extract script {\tt parameter\_extract.py}}\label{app:parameter_extract.py}
%
Python script to extract DAC parameters from CSV files to {\tt rom\/} module
in file {\tt dac\_wrapper.v\/} 
(see Appendix~\ref{app:dac_wrapper.v}).
{\scriptsize\verbatiminput{parameter_extract.py}}

\section{\warning{Teddy's test programs}}
%%
\warning{Teddy, please specify what these programs do 
and what the name of the corresponding {\sf.bit\/} and {\sf.mcs\/} files are.}

\subsection{Verilog source {\tt main.v}}\label{app:main.v}
%
{\scriptsize\verbatiminput{main.v}}

\subsection{FPGA I/O assignment {\tt main\_control.ucf}}\label{app:main_control.ucf}
%
\warning{Teddy, please specify how close this I/O configuration is to a complete description of the Level~0 FPGA pinout.}

{\scriptsize\verbatiminput{main_control.ucf}}

\subsection{Verilog source {\tt DAC\_batch\_load.v}}\label{app:DAC_batch_load.v}
%
{\scriptsize\verbatiminput{DAC_batch_load.v}}

\subsection{Verilog source {\tt USB\_fifo\_control\_R0\_1.v}}\label{app:USB_fifo_control_R0_1.v}
%
{\scriptsize\verbatiminput{USB_fifo_control_R0_1.v}}

\section{FPGA I/O pin assignments}\label{app:IOpins}
%%
\subsection{FPGA I/O pin assignment {\tt main\_io.ucf\/}}\label{app:main_io.ucf}
%
I/O pin assignment from Teddy's latest {\sl IFAT\_FPGA\/} code:
{\scriptsize\verbatiminput{main_io.ucf}}
\subsection{FPGA I/O pin assignment {\tt One\_chip.ucf\/}}\label{app:One_chip.ucf}
%
I/O pin assignment from Jongkil's latest {\sl HiAER\_FPGA\/} code:
{\scriptsize\verbatiminput{One_chip.ucf}}
%
\end{document}
