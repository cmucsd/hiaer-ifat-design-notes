`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: UCSD ISN	Lab
// Engineer: Christoph Maier
//
// Create Date:   18:25:48 08/15/2012
// Design Name:   clk_divider
// Module Name:   C:/Xilinx/projects/IFAT_out_fifo_test/test_divider.v
// Project Name:  dac
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: clk_divider
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_divider;

	// Inputs
	reg clkin;
	reg run;

	// Outputs
	wire done;

	// Instantiate the Unit Under Test (UUT)
	clk_divider uut (
		.clkin(clkin), 
		.run(run), 
		.done(done)
	);

	initial begin
		// Initialize Inputs
		clkin = 0;
		run = 0;
		// Wait 21 ns 
		#21;        
		// Add stimulus here
        run= 1;
        #212;
        run= 0;
	end

    always
        #2.5 clkin= ~clkin;
      
endmodule

